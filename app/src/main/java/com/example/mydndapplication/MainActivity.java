package com.example.mydndapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.mydndapplication.account_analyser.AccountAnalyserActivity;
import com.example.mydndapplication.account_analyser.database.Database;
import com.example.mydndapplication.dice_activities.DiceHeadActivity;
import com.example.mydndapplication.manager_activities.CampaignListViewActivity;
import com.example.mydndapplication.model.DataProvider;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataProvider.readCampaignArrayFromJSON(this);

        Database database = new Database();
        database.init();
    }

    //start dice throw activity
    public void startDieIntent(View view) {
        Intent intent = new Intent(this, DiceHeadActivity.class);
        startActivity(intent);
    }

    //start manager activity
    public void startManagerIntent(View view) {
        Intent intent = new Intent(this, CampaignListViewActivity.class);
        startActivity(intent);
    }

    //start manager activity
    public void startAccountAnalyserIntent(View view) {
        Intent intent = new Intent(this, AccountAnalyserActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DataProvider.writeDataToJSONFile(this);
    }

    //TODO fix that the app saves a JSON file OnHide, or OnDestroypus
    //TODO fix a player beeing added to campaign id 0 in main list, also beeing added to campaign id 1 in main list. ?

    //TODO confirmDeletion messages
    //TODO make all backgrounds
    //TODO make all things look nice AKA. complete presentation
    //TODO add an easter egg
    //TODO fix message saying "List sorted" When there is 1 or less items in the list"
    //TODO add JAVADoc

}
