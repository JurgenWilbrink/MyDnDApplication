package com.example.mydndapplication.model;

public class SV {

    static final String PLAYER_NUMBER = "playernumber";
    static final String PLAYER_NAME = "playername";
    static final String CHARACTER_NAME = "charactername";
    static final String CHARACTER_HP = "characterhp";
    static final String CHARACTER_CLASS = "characterclass";
    static final String CHARACTER_WEAPON = "characterweapon";
    static final String CHARACTER_IMAGE = "characterimage";
    static final String CHARACTER_CUSTOMIMAGE = "charactercustomimage";

    static final String CAMPAIGN_NUMBER = "campaignnumber";
    static final String CAMPAIGN_PLAYER_LIST = "playerlist";
    static final String CAMPAIGN_NAME = "campaignname";
    static final String CAMPAIGN_DESCRIPTION = "campaigndescription";
    static final String CAMPAIGN_WORLD_NAME = "campaignworldname";
    static final String CAMPAIGN_IMAGE = "campaignimage";
    static final String CAMPAIGN_DUNGEON_MASTER = "campaigndungeonmaster";
    static final String CAMPAIGN_CUSTOMIMAGE = "campaigncustomimage";

    public static final String TAG_JSON = "jsondata";
}
