package com.example.mydndapplication.model;

import android.content.Context;
import android.util.Log;

import com.example.mydndapplication.dice_activities.Dice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static android.content.Context.MODE_PRIVATE;


public class DataProvider {

    public static List<CampaignItem> campaignItemList;


    // an empty player template
    public static Player emptyPlayerTemplate = new Player("", "", 1, "", "", "", false);
    public static CampaignItem emptyCampaignTemplate = new CampaignItem(new ArrayList<>(), "", "", "", "", "", false);

    // the purpose of this DataProvider is that the critic of this app doesn't need to add a ton off information him/herself
    static {

        campaignItemList = new ArrayList<>();

        if (false) {

            // This one will funcion as the first added campaign. therefore after this one the "Last added item" can be instantiated
            addItem(new CampaignItem(new ArrayList<>(), "The Colo Choir", "a Campaign full of music", "Nyenja", "choir.png", "Jurgen", false));
            campaignItemList.get(0).addPlayerToList(new Player("Marcel", "Josh", 23, "Bard", "Lyre", "fighter5.jpg", false));

            // Instantiating "LastAddedItem"
            CampaignItem lastAddedCampaign;

            // Adding 7 Calls of Cthulhu for the scroll effect
            for (int i = 0; i < 10; i++) {
                addItem(new CampaignItem(new ArrayList<>(), "Ze Call of Cthulhu #" + (i + 1), "a Campaign full of Tentacles", "OverWorld", "cbook.jpg", "Kyra", false));
                lastAddedCampaign = campaignItemList.get(0); // updating the current added list

                lastAddedCampaign.addPlayerToList(new Player("Steven", "Steve", 24, "Fighter", "Dual Swords", "fighter1.jpg", false));
                lastAddedCampaign.addPlayerToList(new Player("Henk", "Ogmar", 26, "Barbarian", "Great Axe", "fighter2.jpg", false));
                lastAddedCampaign.addPlayerToList(new Player("Piet", "Peter", 200, "Fighter", "Shield", "fighter3.jpg", false));
                lastAddedCampaign.addPlayerToList(new Player("Johan", "Jim", 20, "Sorcerer", "Staff", "mage1.jpg", false));
                lastAddedCampaign.addPlayerToList(new Player("Susan", "Mira", 14, "Cleric", "None", "mage2.jpg", false));
            }

            // This is the genuine campaign i run myself
            addItem(new CampaignItem(new ArrayList<>(), "Dragonic Evolution", "a Campaign full of dragonic stuff", "Nyenja", "dragon.jpg", "Jurgen", false));
            lastAddedCampaign = campaignItemList.get(0); // updating the current added list

            lastAddedCampaign.addPlayerToList(new Player("Marcel", "Darcy", 6, "Bard", "Short Bow", "mage6.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Kyra", "Ushat", 5, "Sorcerer", "Fire Staff", "mage5.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Renate", "Falirya", 7, "Sorcerer", "Dagger", "mage4.jpg", false));

            // This is another Campaign i play in myself
            addItem(new CampaignItem(new ArrayList<>(), "SoulReavers", "a campaign full of idiots :D", "-Unknown-", "soul.jpg", "Kyra", false));
            lastAddedCampaign = campaignItemList.get(0); // updating the current added list

            lastAddedCampaign.addPlayerToList(new Player("Renate", "Luden", 35, "Ranger", "Long bow", "fighter6.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Max", "Grima", 79, "Blood Hunter", "Dual Blade", "mage3.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Jurgen", "Roy", 107, "Fighter/DragonTamer", "Great Axe", "Roy Mustache.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Sanne", "Roywin", 54, "Rogue", "Rapier", "fighter4.jpg", false));

            // And another random Campaign with a lot of players for the scroll effect in the next Intent.
            addItem(new CampaignItem(new ArrayList<>(), "After-Worlds", "a Campaign full of dragonic stuff", "After land", "bot.png", "Master Robot", false));
            lastAddedCampaign = campaignItemList.get(0); // updating the current added list

            lastAddedCampaign.addPlayerToList(new Player("Henk", "WDBD-1", 12, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Joris", "WDBD-2", 13, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Boris", "WDBD-3", 14, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Bas", "WDBD-4", 15, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Sarah", "WDBD-5", 16, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Merel", "WDBD-6", 17, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Sam", "WDBD-7", 13, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Steef", "WDBD-8", 23, "Fighter", "Pulse Gun", "c3po.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Carl", "WDBD-9", 7, "Mechanic", "Repair Staff", "robot.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Ramen", "WDBD-10", 32, "Mechanic", "Repair Staff", "robot.jpg", false));
            lastAddedCampaign.addPlayerToList(new Player("Marcello", "WDBD-11", 52, "Tank", "Shield", "tankybot.png", false));
            lastAddedCampaign.addPlayerToList(new Player("Hugo", "WDBD-12", 72, "Tank", "Shield", "tankybot.png", false));
        }
    }


    private static void addItem(CampaignItem item) {
        campaignItemList.add(0, item);
    }

    public static void setCampaignItem(CampaignItem campaignItem, int id) {
        int setCamp = -1;
        for (int i = 0; i < campaignItemList.size(); i++) {
            if (campaignItemList.get(i).getCampaignNr() == id) {
                setCamp = i;
            }
        }
        campaignItemList.set(setCamp, campaignItem);
    }

    public static int getIdByPosition(int position) {
        return campaignItemList.get(position).getCampaignNr();
    }

    public static CampaignItem findCampaignById(int id) {
        CampaignItem returnCamp = null;
        for (CampaignItem campaignItem : campaignItemList) {
            if (campaignItem.getCampaignNr() == id) {
                returnCamp = campaignItem;
            }
        }
        return returnCamp;
    }

    public static void removeCampaignById(int id) {
        int removeCamp = -1;
        for (int i = 0; i < campaignItemList.size(); i++) {
            if (campaignItemList.get(i).getCampaignNr() == id) {
                removeCamp = i;
            }
        }
        campaignItemList.remove(removeCamp);
    }

    public static boolean doesCampaignExistById(int id) {
        boolean returner = false;
        for (CampaignItem item : campaignItemList) {
            if (item.getCampaignNr() == id) {
                returner = true;
                Log.i("checks", "doesCampaignExistById: found one!");
            }
        }
        return returner;
    }

    public static List<Dice> diceItems;

    static {
        diceItems = new ArrayList<>();
        diceItems.add(0, new Dice(4));
        diceItems.add(1, new Dice(6));
        diceItems.add(2, new Dice(8));
        diceItems.add(3, new Dice(10));
        diceItems.add(4, new Dice(12));
        diceItems.add(5, new Dice(20));
        diceItems.add(6, new Dice(100));
        diceItems.add(7, new Dice(1000));
    }

    private static String TAG = "jsondata";
    private static String FILE_NAME = "campaignsavedata.json";

    public static void readCampaignArrayFromJSON(Context context) {
        Log.i(TAG, "readCampaignArrayFromJSON: InFrontOfArray " + campaignItemList.toString());

        Path p = Paths.get(FILE_NAME);

        JSONArray jsonArray;
        ArrayList<CampaignItem> campaignItems = new ArrayList<>();
        try {
            String jsonString = null;
            //jsonString = new String(Files.readAllBytes(Paths.get(FILE_NAME)));


            Scanner sc = new Scanner("./" + FILE_NAME);
            jsonString = sc.useDelimiter("\\A").next();
            sc.close();

            Log.i(TAG, "readCampaignArrayFromJSON: origianl read file: " + jsonString);

            jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject campaignJSONObject = jsonArray.getJSONObject(i);

                JSONArray playersJSONArray = campaignJSONObject.getJSONArray("players");
                ArrayList<Player> playerlist = new ArrayList<>();

                //getting player objects from json objects
                for (int j = 0; j < playersJSONArray.length(); j++) {
                    JSONObject player = playersJSONArray.getJSONObject(i);

                    String playerNameString = player.getString(SV.PLAYER_NAME);
                    String characterNameString = player.getString(SV.CHARACTER_NAME);
                    int characterHP = player.getInt(SV.CHARACTER_HP);
                    String characterClass = player.getString(SV.CHARACTER_CLASS);
                    String characterWeapon = player.getString(SV.CHARACTER_WEAPON);
                    String characterImage = player.getString(SV.CHARACTER_IMAGE);
                    boolean customImage = player.getBoolean(SV.CHARACTER_CUSTOMIMAGE);

                    playerlist.add(new Player(playerNameString, characterNameString,
                            characterHP, characterClass, characterWeapon, characterImage, customImage));

                }

                String campaignName = campaignJSONObject.getString(SV.CAMPAIGN_NAME);
                String campaignDescription = campaignJSONObject.getString(SV.CAMPAIGN_DESCRIPTION);
                String campaignWorldName = campaignJSONObject.getString(SV.CAMPAIGN_WORLD_NAME);
                String campaignimage = campaignJSONObject.getString(SV.CAMPAIGN_IMAGE);
                String campaignDM = campaignJSONObject.getString(SV.CAMPAIGN_DUNGEON_MASTER);
                boolean campaignCustomImage = campaignJSONObject.getBoolean(SV.CAMPAIGN_CUSTOMIMAGE);

                CampaignItem campaignItem = new CampaignItem(playerlist, campaignName, campaignDescription,
                        campaignWorldName, campaignimage, campaignDM, campaignCustomImage);

                Log.i(TAG, "readCampaignArrayFromJSON: campitem: " + campaignItem.toString());

                campaignItems.add(campaignItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        campaignItemList = campaignItems;

        Log.i(TAG, "readCampaignArrayFromJSON: afterArray: " + campaignItemList.toString());
    }

    public static void writeDataToJSONFile(Context context) {
        JSONArray jsonArray = new JSONArray();

        Log.i(TAG, "writeDataToJSONFile: before: " + campaignItemList.toString());

        for (CampaignItem campaignItem : campaignItemList) { // add weapons that already were on the list to jsonArray
            jsonArray.put(campaignItem.toJsonObject());
        }

        Log.i(TAG, "writeDataToJSONFile: jsonArray after adding " + jsonArray);

        try { // save jsonArray
            String jsonString = jsonArray.toString(2);

            Log.i(TAG, "writeDataToJSONFile: jsonString: '" + jsonString + "'");

            //Files.write(Paths.get(FILE_NAME), jsonString.getBytes());

            try {
                OutputStream os = context.openFileOutput(FILE_NAME, MODE_PRIVATE);
                BufferedWriter lout = new BufferedWriter(new OutputStreamWriter(os));
                lout.write(jsonString);
                lout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.i(TAG, "writeDataToJSONFile: written '" + jsonString + "' to file");

//        } catch (IOException ioe) {
//            System.out.println(" > Something Went Wrong: " + ioe.getMessage());
//            ioe.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
