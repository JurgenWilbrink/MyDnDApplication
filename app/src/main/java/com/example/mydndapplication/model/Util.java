package com.example.mydndapplication.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class Util {

    public static void setImage(CampaignItem item, ImageView imageView, Context context) {
        if (item.isCustomImage()) {
            // the image String is a file path so it can be decoded to an image using:
            imageView.setImageBitmap(BitmapFactory.decodeFile(item.getImage()));
        } else {
            // else the image needs to be streamed from the assets folder.
            // this is the case with images from campaigns implanted beforehand.
            InputStream inputStream = null;
            try {
                String imagefile = item.getImage();

                inputStream = context.getAssets().open(imagefile);
                Drawable d = Drawable.createFromStream(inputStream, null);
                imageView.setImageDrawable(d);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void setImage(Player item, ImageView imageView, Context context) {
        if (item.isCustomImage()) {
            // the image String is a file path so it can be decoded to an image using:
            imageView.setImageBitmap(BitmapFactory.decodeFile(item.getImage()));
        } else {
            // else the image needs to be streamed from the assets folder.
            // this is the case with images from campaigns implanted beforehand.
            InputStream inputStream = null;
            try {
                String imagefile = item.getImage();

                inputStream = context.getAssets().open(imagefile);
                Drawable d = Drawable.createFromStream(inputStream, null);
                imageView.setImageDrawable(d);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String rollDiceWithMod(int amountOfDice, int maxSides, int amountofMod, boolean modMode) {
        //Initializing Random
        Random ran = new Random();

        //Initializing some values
        int thisRunInt = 0;
        int totalInt = 0;
        String returningString = "";
        String modifier = "";

        if (amountOfDice > 0) {
            //Starting Die Throw Loop
            for (int i = 0; i < amountOfDice; i++) {

                //Initializing some more values
                String nat = "";
                String komma = ", ";

                //Throwing a single dice
                thisRunInt = ran.nextInt(maxSides) + 1;

                //if the dice is a d20, and the value is 20 or 1, place "nat. " in front of it, (for Natural)
                if (maxSides == 20 && (thisRunInt == 20 || thisRunInt == 1) && amountOfDice == 1) {
                    nat = "nat. ";
                }

                //the last number doesn't need a komma, fixed with this.
                if (i == amountOfDice - 1) {
                    komma = "";
                }

                //adding this throw to returning result.
                returningString = returningString + (nat + thisRunInt + komma);

                //adding thsi throw to total of throws.
                totalInt = totalInt + thisRunInt;
            }

            //if dice is a d20, and result is 1, and only 1 die is thrown, no modifiers
            if (maxSides == 20 && thisRunInt == 1 && amountOfDice == 1 && amountofMod > 0) {
                returningString = returningString + " (no mods for a nat. 1)";
            } else if (amountofMod > 0) { //if modifier value > 0 display this, else ignore.
                if (modMode) { // positive modifier
                    modifier = "+" + amountofMod;
                    totalInt = totalInt + amountofMod;
                } else { // negative modifier
                    modifier = "-" + amountofMod;
                    totalInt = totalInt - amountofMod;
                    // making sure if total comes below zero, it will be displayed as 1.
                    if (totalInt < 1) {
                        totalInt = 1;
                    }
                }
            }

            //Adding the thrown-die-signature, for example 3d10.
            returningString = "Roll(" + amountOfDice + "d" + maxSides + ")" + modifier + ":\n" + returningString;

            //Displaying total value, if more then 1 die was thrown or mod value > 0.
            if (amountOfDice > 1 || amountofMod > 0) {
                returningString = returningString + ("\nTotal: " + totalInt);
            }

        } else {
            returningString = "* You tried to throw 0 die, try again...";
        }

        // returning String with all the throws.
        return returningString;
    }

}
