package com.example.mydndapplication.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CampaignItem {

    private int campaignNr;
    private ArrayList<Player> playerList;
    private String name;
    private String description;
    private String worldName;
    private String image;
    private String dungeonMaster;
    private boolean customImage;

    private static int lastAssignedStudentNumber = 0;

    public CampaignItem(ArrayList<Player> playerList, String name, String description, String worldName, String image, String DM, boolean customImage) {
        lastAssignedStudentNumber++;
        this.campaignNr = lastAssignedStudentNumber;
        Log.i("checks", "CampaignItem: " + lastAssignedStudentNumber);
        this.playerList = playerList;
        this.name = name;
        this.description = description;
        this.worldName = worldName;
        this.image = image;
        this.dungeonMaster = DM;
        this.customImage = customImage;
    }

    public boolean isCustomImage() {
        return customImage;
    }

    public void setCustomImage(boolean customImage) {
        this.customImage = customImage;
    }

    public String getDungeonMaster() {
        return dungeonMaster;
    }

    public void setDungeonMaster(String dungeonMaster) {
        this.dungeonMaster = dungeonMaster;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCampaignNr() {
        return campaignNr;
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public Player findPlayerById(int id){
        Player returnPlayer = null;
        for (Player player : playerList) {
            if (player.getPlayerNr() == id) {
                returnPlayer = player;
            }
        }
        return returnPlayer;
    }

    public int findIdByPosition(int position){
        return playerList.get(position).getPlayerNr();
    }

    public int getPlayerListSize(){
        return playerList.size();
    }

    public void addPlayerToList(Player item){
        playerList.add(item);
    }

    public void setPlayerToList(Player item, int id){
        int position = -1;
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getPlayerNr() == id) {
                position = i;
            }
        }
        playerList.set(position, item);
    }

    public void removePlayerById(int id){
        int position = -1;
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getPlayerNr() == id) {
                position = i;
            }
        }
        playerList.remove(position);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorldName() {
        return worldName;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    public JSONObject toJsonObject(){
        JSONObject obj = new JSONObject();

        try {
            obj.put(SV.CAMPAIGN_NUMBER, this.campaignNr);

            JSONArray jsonplayerlist = new JSONArray();

            for (Player player : this.playerList) {
                jsonplayerlist.put(player.toJsonObject());
            }

            obj.put(SV.CAMPAIGN_PLAYER_LIST, jsonplayerlist);

            obj.put(SV.CAMPAIGN_NAME, this.name);
            obj.put(SV.CAMPAIGN_DESCRIPTION, this.description);
            obj.put(SV.CAMPAIGN_WORLD_NAME, this.worldName);
            obj.put(SV.CAMPAIGN_IMAGE, this.image);
            obj.put(SV.CAMPAIGN_DUNGEON_MASTER, this.dungeonMaster);
            obj.put(SV.CAMPAIGN_CUSTOMIMAGE, this.customImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }
}

