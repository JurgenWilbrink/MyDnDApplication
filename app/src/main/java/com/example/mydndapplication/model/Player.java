package com.example.mydndapplication.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Player {

    private int playerNr;
    private String playerName;
    private String characterName;
    private int characterHP;
    private String characterClass;
    private String characterWeapon;
    private String image;
    private boolean customImage;

    private static int lastAssignedPlayerNumber = 0;

    public Player(String playerName, String characterName, int characterHP, String getCharacterClass, String characterWeapon, String image, boolean customImage) {
        lastAssignedPlayerNumber++;
        this.playerNr = lastAssignedPlayerNumber;
        Log.i("checks", "Player: " + lastAssignedPlayerNumber);
        this.playerName = playerName;
        this.characterName = characterName;
        this.characterHP = characterHP;
        this.characterClass = getCharacterClass;
        this.characterWeapon = characterWeapon;
        this.image = image;
        this.customImage = customImage;
    }

    public boolean isCustomImage() {
        return customImage;
    }

    public void setCustomImage(boolean customImage) {
        this.customImage = customImage;
    }

    public int getPlayerNr() {
        return playerNr;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public int getCharacterHP() {
        return characterHP;
    }

    public void setCharacterHP(int characterHP) {
        this.characterHP = characterHP;
    }

    public String getCharacterClass() {
        return characterClass;
    }

    public String getCharacterWeapon() {
        return characterWeapon;
    }

    public void setCharacterWeapon(String characterWeapon) {
        this.characterWeapon = characterWeapon;
    }

    public void setCharacterClass(String characterClass) {
        this.characterClass = characterClass;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public JSONObject toJsonObject(){
        JSONObject obj = new JSONObject();

        try {
            obj.put(SV.PLAYER_NUMBER, this.playerNr);
            obj.put(SV.PLAYER_NAME, this.playerName);
            obj.put(SV.CHARACTER_NAME, this.characterName);
            obj.put(SV.CHARACTER_HP, this.characterHP);
            obj.put(SV.CHARACTER_CLASS, this.characterClass);
            obj.put(SV.CHARACTER_WEAPON, this.characterWeapon);
            obj.put(SV.CHARACTER_IMAGE, this.image);
            obj.put(SV.CHARACTER_CUSTOMIMAGE, this.customImage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }
}
