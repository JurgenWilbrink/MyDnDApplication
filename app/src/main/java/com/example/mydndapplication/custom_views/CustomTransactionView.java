package com.example.mydndapplication.custom_views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.account_analyser.Transaction;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.Util;

public class CustomTransactionView extends ConstraintLayout {

    private TextView transactionField;

    public CustomTransactionView(Context context) {
        super(context);
        init();
    }

    public CustomTransactionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTransactionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.transaction_list_item, this);

        this.transactionField = findViewById(R.id.transactionItem);
    }

    @SuppressLint("SetTextI18n")
    public void setTransaction(Transaction transaction){
        this.transactionField.setText(transaction.amount + " " + transaction.currency);
    }
}
