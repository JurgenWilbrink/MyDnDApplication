package com.example.mydndapplication.custom_views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.InputFilterMinMax;
import com.example.mydndapplication.model.Player;

import static com.example.mydndapplication.model.DataProvider.emptyPlayerTemplate;

public class GeneralPlayerInformationView extends ConstraintLayout {

    private EditText playerNameInput, chrNameInput, chrHPInput, chrClassInput, chrWeaponInput;

    public GeneralPlayerInformationView(Context context) {
        super(context);
        init();
    }

    public GeneralPlayerInformationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GeneralPlayerInformationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.general_player_change_or_add_view, this);

        this.playerNameInput = findViewById(R.id.campaignNameInput);
        this.chrNameInput = findViewById(R.id.chrNameInput);
        this.chrClassInput = findViewById(R.id.classInput);
        this.chrHPInput = findViewById(R.id.hpInput);
        this.chrWeaponInput = findViewById(R.id.weaponInput);
    }

    public void setGeneralPlayerInfo(Player playeritem) {
        this.playerNameInput.setText(playeritem.getPlayerName());
        this.chrNameInput.setText(playeritem.getCharacterName());
        this.chrClassInput.setText(playeritem.getCharacterClass());
        this.chrHPInput.setText(String.valueOf(playeritem.getCharacterHP()));
        this.chrWeaponInput.setText(playeritem.getCharacterWeapon());

        // hp input filter so hp cant be below 1 or above 999
        this.chrHPInput.setFilters(new InputFilter[]{new InputFilterMinMax("1", "999")});
    }

    public Player getFieldInputAsPlayerItem() {
        return new Player(
                String.valueOf(playerNameInput.getText()),
                String.valueOf(chrNameInput.getText()),
                Integer.parseInt(String.valueOf(chrHPInput.getText())),
                String.valueOf(chrClassInput.getText()),
                String.valueOf(chrWeaponInput.getText()),
                emptyPlayerTemplate.getImage(),
                emptyPlayerTemplate.isCustomImage());
    }

}
