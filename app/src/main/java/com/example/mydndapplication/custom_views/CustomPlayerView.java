package com.example.mydndapplication.custom_views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.Player;
import com.example.mydndapplication.manager_activities.DrawingCharacterHP;
import com.example.mydndapplication.model.Util;

public class CustomPlayerView extends ConstraintLayout {

    private TextView namePlayerInput, nameChrInput, chrClass, chrWeapon;
    private DrawingCharacterHP drawingCharacterHP;
    private ImageView chrImage;


    public CustomPlayerView(Context context) {
        super(context);
        init();
    }

    public CustomPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.player_list_item, this);

        this.namePlayerInput = findViewById(R.id.campaignNameInput);
        this.nameChrInput = findViewById(R.id.chrNameInput);
        this.drawingCharacterHP = findViewById(R.id.drawingCharacterHP);
        this.chrClass = findViewById(R.id.chrClassInput);
        this.chrWeapon = findViewById(R.id.chrWeaponInput);
        this.chrImage = findViewById(R.id.chrImageInput);
    }

    public void setPlayer(Player player){
        this.namePlayerInput.setText(player.getPlayerName());
        this.nameChrInput.setText(player.getCharacterName());
        this.drawingCharacterHP.setGettingChrHP(player.getCharacterHP());
        this.chrClass.setText(player.getCharacterClass());
        this.chrWeapon.setText(player.getCharacterWeapon());

        Util.setImage(player, this.chrImage, getContext());
    }
}
