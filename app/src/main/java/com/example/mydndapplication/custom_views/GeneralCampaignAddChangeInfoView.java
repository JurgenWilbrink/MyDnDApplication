package com.example.mydndapplication.custom_views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;

import static com.example.mydndapplication.model.DataProvider.emptyCampaignTemplate;

public class GeneralCampaignAddChangeInfoView extends ConstraintLayout {

    private EditText campaignTitleInput, dmInput, worldInput, descriptionInput;

    public GeneralCampaignAddChangeInfoView(Context context) {
        super(context);
        init();
    }

    public GeneralCampaignAddChangeInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GeneralCampaignAddChangeInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.general_campaign_change_or_add_view, this);

        this.campaignTitleInput = findViewById(R.id.campaignNameInput);
        this.dmInput = findViewById(R.id.dmInput);
        this.worldInput = findViewById(R.id.worldInput);
        this.descriptionInput = findViewById(R.id.descriptionInput);
    }

    public void setCampaignItemToFields(CampaignItem campaignItem){
        this.campaignTitleInput.setText(campaignItem.getName());
        this.dmInput.setText(campaignItem.getDungeonMaster());
        this.worldInput.setText(campaignItem.getWorldName());
        this.descriptionInput.setText(campaignItem.getDescription());
    }

    public CampaignItem getFieldInputAsCampaignItem(){
        return new CampaignItem(
                emptyCampaignTemplate.getPlayerList(),
                String.valueOf(campaignTitleInput.getText()),
                String.valueOf(descriptionInput.getText()),
                String.valueOf(worldInput.getText()),
                emptyCampaignTemplate.getImage(),
                String.valueOf(dmInput.getText()),
                emptyCampaignTemplate.isCustomImage());
    }
}
