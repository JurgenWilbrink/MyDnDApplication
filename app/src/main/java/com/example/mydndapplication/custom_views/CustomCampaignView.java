package com.example.mydndapplication.custom_views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.Util;

public class CustomCampaignView extends ConstraintLayout {

    private TextView nameInput, dmInput, worldInput, nrOfPlayersInput;
    private ImageView campaignImageView;

    public CustomCampaignView(Context context) {
        super(context);
        init();
    }

    public CustomCampaignView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomCampaignView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.campaign_list_item, this);

        this.nameInput = findViewById(R.id.campaignNameInput);
        this.dmInput = findViewById(R.id.dmInputField);
        this.worldInput = findViewById(R.id.worldInputField);
        this.nrOfPlayersInput = findViewById(R.id.playerAmountInputField);
        this.campaignImageView = findViewById(R.id.campaignDetailImage);
    }

    public void setCampaign(CampaignItem campaignItem){
        this.nameInput.setText(campaignItem.getName());
        this.dmInput.setText(campaignItem.getDungeonMaster());
        this.worldInput.setText(campaignItem.getWorldName());
        this.nrOfPlayersInput.setText(String.valueOf(campaignItem.getPlayerListSize()));

        Util.setImage(campaignItem, this.campaignImageView, getContext());
    }
}
