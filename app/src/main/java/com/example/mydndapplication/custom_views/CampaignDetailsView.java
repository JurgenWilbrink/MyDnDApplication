package com.example.mydndapplication.custom_views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;
import com.example.mydndapplication.model.Util;

public class CampaignDetailsView extends ConstraintLayout {

    private TextView campaignNameInput, amountOfPlayerInput, worldNameInput, dmInput, descriptionInput;
    private ImageView campaignImage;

    public CampaignDetailsView(Context context) {
        super(context);
        init();
    }

    public CampaignDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CampaignDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.top_side_of_campaign_details_as_custom_view, this);

        this.campaignNameInput = findViewById(R.id.campaignNameInput);
        this.amountOfPlayerInput = findViewById(R.id.playerNrInput);
        this.worldNameInput = findViewById(R.id.worldInput);
        this.dmInput = findViewById(R.id.dmInputField);
        this.descriptionInput = findViewById(R.id.descriptionInput);

        this.campaignImage = findViewById(R.id.campaignDetailImage);
    }

    public void setCampaignDetails(int campaignId){
        CampaignItem currentCampaignItem = DataProvider.findCampaignById(campaignId);

        Log.i("CCVchecks", "setCampaignDetails: " + currentCampaignItem.getName() + " + " + currentCampaignItem.getPlayerListSize() + " + " + currentCampaignItem.getWorldName());

        this.campaignNameInput.setText(currentCampaignItem.getName());
        this.amountOfPlayerInput.setText(String.valueOf(currentCampaignItem.getPlayerListSize()));
        this.worldNameInput.setText(currentCampaignItem.getWorldName());
        this.dmInput.setText(currentCampaignItem.getDungeonMaster());
        this.descriptionInput.setText(currentCampaignItem.getDescription());

        Util.setImage(currentCampaignItem, this.campaignImage, getContext());
    }
}
