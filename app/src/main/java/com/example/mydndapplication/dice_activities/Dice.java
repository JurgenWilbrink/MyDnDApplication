package com.example.mydndapplication.dice_activities;
public class Dice {

    // I've made this Dice model class as some sort of a template, because my app can throw multiple dice at the same time.
    // My every time a dice is thrown it "copies" this dice, and throws it.
    // by "copies" I mean that it takes the max amount of sides of this preSet dice and generates a random number with a bound of that max amount of sides.

    private int maxSides;

    public Dice(int maxSides) {
        this.maxSides = maxSides;
    }

    public int getMaxSides() {
        return maxSides;
    }
}
