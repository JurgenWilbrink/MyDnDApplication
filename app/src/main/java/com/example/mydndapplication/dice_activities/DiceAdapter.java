package com.example.mydndapplication.dice_activities;

import android.content.Context;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.InputFilterMinMax;
import com.example.mydndapplication.model.Util;

import java.util.List;

public class DiceAdapter extends ArrayAdapter<Dice> {

    private LayoutInflater mInflater;

    public DiceAdapter(Context context, List<Dice> dieList) {
        super(context, R.layout.dice_list_item, dieList);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.dice_list_item, parent, false);
        }

        // getting fields
        EditText aOfDice = convertView.findViewById(R.id.amountOfDiceEditable);
        EditText aOfMod = convertView.findViewById(R.id.amountOfModifierEditable);
        TextView dTextField = convertView.findViewById(R.id.dddTextField);
        Button rollButton = convertView.findViewById(R.id.rollButton);
        ToggleButton modfierMode = convertView.findViewById(R.id.modifierModeButton);

        // filters for the edit text (numeric) so that there can not be more then 3 digits
        aOfDice.setFilters(new InputFilter[]{new InputFilterMinMax("0", "999")});
        aOfMod.setFilters(new InputFilter[]{new InputFilterMinMax("0", "999")});

        // getting dice item
        Dice item = getItem(position);
        // getting max sides from current dice
        int maxSides = item.getMaxSides();

        // setting max sides to field
        dTextField.setText("d" + String.valueOf(maxSides));

        // setting 1 text to amount of dice field(s)
        aOfDice.setText("1");

        // and setting a 0 HINT to amount of modifier field(s)
        aOfMod.setHint("0");
        aOfMod.setText("");

        // on click listener for the ROLL button.
        rollButton.setOnClickListener(view -> {
            boolean modMode = modfierMode.isChecked();
            int amountOfDice = 0;
            int amountofMod = 0;

            // testing if fields are not 0
            if (!(String.valueOf(aOfDice.getText()).isEmpty())) {
                amountOfDice = Integer.parseInt(String.valueOf(aOfDice.getText()));
            }

            if (!(String.valueOf(aOfMod.getText()).isEmpty())) {
                amountofMod = Integer.parseInt(String.valueOf(aOfMod.getText()));
            }

            // throwing dice with util class and getting a sring of thrown dice back.
            String diceThrows = Util.rollDiceWithMod(amountOfDice, maxSides, amountofMod, modMode);

            //logging
            Log.i("dices", "getView: " + diceThrows);
            Log.i("dices-modText", "getView: modText + Pos " + aOfMod.getText() + " " + position);

            //updating Output Field to the thrown dices
            //I know events would be better for the next version, but I would have need to spent even more time on this application.
            //and i have other school work to do... therefor i use:
            DiceHeadActivity.placeOutputInOutputField(diceThrows);
        });

        return convertView;
    }

}
