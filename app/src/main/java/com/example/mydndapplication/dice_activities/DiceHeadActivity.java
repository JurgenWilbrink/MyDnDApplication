package com.example.mydndapplication.dice_activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.DataProvider;

public class DiceHeadActivity extends AppCompatActivity {

    private DiceAdapter diceAdapter;
    private ListView diceList;
    private static TextView outputField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice_head);

        // getting fields
        diceList = findViewById(R.id.diceList);
        outputField = findViewById(R.id.outputField);

        // setting scroll for the output field
        outputField.setMovementMethod(new ScrollingMovementMethod());

        // setting dice adepter
        diceAdapter = new DiceAdapter(this, DataProvider.diceItems);
        diceList.setAdapter(diceAdapter);

        //getting support for the back button in the top left corner
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void resetDiceListValues(View view) {
        //resetting values of the dices
        diceList.setAdapter(diceAdapter);

        //resetting outputField text as well
        outputField.setText(getString(R.string.explanation));
    }

    public static void placeOutputInOutputField(String diceThrows){
        //Actually: theFieldOfOutput.setText("\n" + diceThrows + "\n---------------" + String.valueOf(theFieldOfOutput.getText()));
        outputField.setText(String.format("\n%s\n---------------%s", diceThrows, String.valueOf(outputField.getText())));
    }


}
