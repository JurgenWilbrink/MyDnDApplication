package com.example.mydndapplication.dice_activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class DrawingDiceView extends View {

    private Paint paintBlack = new Paint();

    public DrawingDiceView(Context context) {
        super(context);
        init();
    }

    public DrawingDiceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingDiceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DrawingDiceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        paintBlack.setColor(Color.WHITE);
        paintBlack.setStrokeWidth(5f);
    }

    float n = 0;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // makes the dice twist.
        canvas.translate(getWidth()/2, getHeight()/2);
        canvas.rotate(n++);
        canvas.translate(-getWidth()/2, -getHeight()/2);

        // getting information from the canvas
        float width = (float) (getWidth() / 10) * 9;
        float height = (float) (getHeight() / 10) * 9;
        float marge = width / 20;
        float invisside = ((height / 100) * 47) / 2;

        // initializing points of the Hexagon
        // Initialized as:
        //
        //     0
        //  5     1
        //  4     2
        //     3
        //
        float[][] hexaXYs = {
                {width / 2 + marge, marge},
                {width + marge, invisside + (marge * 2)},
                {width + marge, invisside * 3 + (marge * 2)},
                {width / 2 + marge, height + (marge * 2)},
                {marge, invisside * 3 + (marge * 2)},
                {marge, invisside + (marge * 2)}
        };

        // drawing lines between the points of the hexagon
        canvas.drawLine(hexaXYs[0][0], hexaXYs[0][1], hexaXYs[1][0], hexaXYs[1][1], paintBlack);
        canvas.drawLine(hexaXYs[1][0], hexaXYs[1][1], hexaXYs[2][0], hexaXYs[2][1], paintBlack);
        canvas.drawLine(hexaXYs[2][0], hexaXYs[2][1], hexaXYs[3][0], hexaXYs[3][1], paintBlack);
        canvas.drawLine(hexaXYs[3][0], hexaXYs[3][1], hexaXYs[4][0], hexaXYs[4][1], paintBlack);
        canvas.drawLine(hexaXYs[4][0], hexaXYs[4][1], hexaXYs[5][0], hexaXYs[5][1], paintBlack);
        canvas.drawLine(hexaXYs[5][0], hexaXYs[5][1], hexaXYs[0][0], hexaXYs[0][1], paintBlack);

        // initializing points of the inner Triangle
        // Initialized as:
        //
        //     0
        //
        //  2     1
        //
        float[][] triangleXYs = {
                {width / 2 + marge, height / 5 + marge},
                {(width / 19) * 4 + (width / 19) * 11 + marge, (height / 10) * 7 + marge},
                {(width / 19) * 4 + marge, (height / 10) * 7 + marge}
        };

        // drawing lines between the points of the triangle
        canvas.drawLine(triangleXYs[0][0], triangleXYs[0][1], triangleXYs[1][0], triangleXYs[1][1], paintBlack);
        canvas.drawLine(triangleXYs[1][0], triangleXYs[1][1], triangleXYs[2][0], triangleXYs[2][1], paintBlack);
        canvas.drawLine(triangleXYs[2][0], triangleXYs[2][1], triangleXYs[0][0], triangleXYs[0][1], paintBlack);

        // drawing lines from top of triangle
        canvas.drawLine(triangleXYs[0][0], triangleXYs[0][1], hexaXYs[0][0], hexaXYs[0][1], paintBlack);
        canvas.drawLine(triangleXYs[0][0], triangleXYs[0][1], hexaXYs[1][0], hexaXYs[1][1], paintBlack);
        canvas.drawLine(triangleXYs[0][0], triangleXYs[0][1], hexaXYs[5][0], hexaXYs[5][1], paintBlack);

        // drawing lines from right of triangle
        canvas.drawLine(triangleXYs[1][0], triangleXYs[1][1], hexaXYs[1][0], hexaXYs[1][1], paintBlack);
        canvas.drawLine(triangleXYs[1][0], triangleXYs[1][1], hexaXYs[2][0], hexaXYs[2][1], paintBlack);
        canvas.drawLine(triangleXYs[1][0], triangleXYs[1][1], hexaXYs[3][0], hexaXYs[3][1], paintBlack);

        // drawing lines from left of triangle
        canvas.drawLine(triangleXYs[2][0], triangleXYs[2][1], hexaXYs[3][0], hexaXYs[3][1], paintBlack);
        canvas.drawLine(triangleXYs[2][0], triangleXYs[2][1], hexaXYs[4][0], hexaXYs[4][1], paintBlack);
        canvas.drawLine(triangleXYs[2][0], triangleXYs[2][1], hexaXYs[5][0], hexaXYs[5][1], paintBlack);

        // setting Text size
        paintBlack.setTextSize((width / 19) * 4);

        // initializing Text XY
        float[] textXYs = {width / 2 - (paintBlack.getTextSize() / 2) + marge, (height / 38) * 23 + marge};

        // Drawing Text
        canvas.drawText("20", textXYs[0], textXYs[1], paintBlack);
        invalidate();
    }

}
