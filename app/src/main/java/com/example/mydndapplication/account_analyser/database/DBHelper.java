package com.example.mydndapplication.account_analyser.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.support.annotation.Nullable;

import com.example.mydndapplication.account_analyser.StringTransaction;
import com.example.mydndapplication.account_analyser.models.Banks;
import com.example.mydndapplication.account_analyser.models.MyCurrency;
import com.example.mydndapplication.account_analyser.models.TransRef;

import java.time.LocalDate;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";

    // Transactions table and columns
    public static final String TRANSACTIONS_TABLE_NAME = "transactions";
    public static final String TRANSACTIONS_COLUMN_ID = "id";
    public static final String TRANSACTIONS_COLUMN_IBANBBAN = "ibanbban";                               // "NL29RABO0111095379"
    public static final String TRANSACTIONS_COLUMN_CURRENCY = "currency";                               // "EUR"
    public static final String TRANSACTIONS_COLUMN_BIC = "bic";                                         // "RABONL2U"
    public static final String TRANSACTIONS_COLUMN_FOLLOWNR = "follownr";                               // "000000000000004243"
    public static final String TRANSACTIONS_COLUMN_DATE = "date";                                       // "2023-01-01"
    public static final String TRANSACTIONS_COLUMN_INTERESTDATE = "interestdate";                       // "2023-01-01"
    public static final String TRANSACTIONS_COLUMN_AMOUNT = "amount";                                   // "-44,35"
    public static final String TRANSACTIONS_COLUMN_BALANCEAFTERTRN = "balanceaftertrn";                 // "+610,53"
    public static final String TRANSACTIONS_COLUMN_OPPOSINGPARTYIBANBBAN = "opposingpartyibanbban";     // "NL35RABO0117713678"
    public static final String TRANSACTIONS_COLUMN_OPPOSINGPARTYNAME = "opposingpartyname";             // "Grillroom Sahara via PAY.nl"
    public static final String TRANSACTIONS_COLUMN_EVENTUALPARTYNAME = "eventualpartyname";             // ""
    public static final String TRANSACTIONS_COLUMN_INTERMEDIATEPARTYNAME = "intermediatepartyname";     // ""
    public static final String TRANSACTIONS_COLUMN_OPPOSINGPARTYBIC = "opposingpartybic";               // "RABONL2U"
    public static final String TRANSACTIONS_COLUMN_CODE = "code";                                       // "id"
    public static final String TRANSACTIONS_COLUMN_BATCHID = "batchid";                                 // ""
    public static final String TRANSACTIONS_COLUMN_TRANSACTIONREFERENCE = "transactionreference";       // "01-01-2023 17:19 0370589954576968"
    public static final String TRANSACTIONS_COLUMN_AUTHORIZATIONIDENTIFIER = "authorizationidentifier"; // ""
    public static final String TRANSACTIONS_COLUMN_CREDITORID = "creditorid";                           // ""
    public static final String TRANSACTIONS_COLUMN_PAYMENTREFERENCE = "paymentreference";               // ""
    public static final String TRANSACTIONS_COLUMN_DESCRIPTION1 = "description1";                       // "1961107206X69e4d 0370589954576968 867-12776-Grillroom Sahara x5379 pasnr.044"
    public static final String TRANSACTIONS_COLUMN_DESCRIPTION2 = "description2";                       // " "
    public static final String TRANSACTIONS_COLUMN_DESCRIPTION3 = "description3";                       // ""
    public static final String TRANSACTIONS_COLUMN_REASONRETURN = "reasonreturn";                       // ""
    public static final String TRANSACTIONS_COLUMN_ORIGINALAMOUNT = "originalamount";                   // ""
    public static final String TRANSACTIONS_COLUMN_ORIGINALCURRENCY = "originalcurrency";               // ""
    public static final String TRANSACTIONS_COLUMN_COURSE = "course";                                   // ""

    //Labels table and columns
    public static final String LABELS_TABLE_NAME = "labels";
    public static final String LABELS_COLUMN_ID = "labels";
    public static final String LABELS_COLUMN_NAME = "name";
    public static final String LABELS_COLUMN_COLOR = "color";

    //Binding table for Labels and columns
    public static final String LABELED_TRANSACTIONS_TABLE_NAME = "labeledtransactions";
    public static final String LABELED_TRANSACTIONS_COLUMN_ID = "id";
    public static final String LABELED_TRANSACTIONS_COLUMN_TRANSACTION_ID = "transactionid";
    public static final String LABELED_TRANSACTIONS_COLUMN_LABEL_ID = "labelid";

//    public static final String CONTACTS_COLUMN_ID = "id";
//    public static final String CONTACTS_COLUMN_NAME = "name";
//    public static final String CONTACTS_COLUMN_EMAIL = "email";
//    public static final String CONTACTS_COLUMN_STREET = "street";
//    public static final String CONTACTS_COLUMN_CITY = "place";
//    public static final String CONTACTS_COLUMN_PHONE = "phone";

    private HashMap hp;

    public DBHelper(@Nullable Context context, @Nullable String name,
                    @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TRANSACTIONS_TABLE_NAME + "(" +
                        TRANSACTIONS_COLUMN_ID + " INTEGER PRIMARY KEY," +
                        TRANSACTIONS_COLUMN_IBANBBAN + " TEXT," +
                        TRANSACTIONS_COLUMN_CURRENCY + " TEXT," +
                        TRANSACTIONS_COLUMN_BIC + " TEXT," +
                        TRANSACTIONS_COLUMN_FOLLOWNR + " TEXT," +
                        TRANSACTIONS_COLUMN_DATE + " TEXT," +
                        TRANSACTIONS_COLUMN_INTERESTDATE + " TEXT," +
                        TRANSACTIONS_COLUMN_AMOUNT + " TEXT," +
                        TRANSACTIONS_COLUMN_BALANCEAFTERTRN + " TEXT," +
                        TRANSACTIONS_COLUMN_OPPOSINGPARTYIBANBBAN + " TEXT," +
                        TRANSACTIONS_COLUMN_OPPOSINGPARTYNAME + " TEXT," +
                        TRANSACTIONS_COLUMN_EVENTUALPARTYNAME + " TEXT," +
                        TRANSACTIONS_COLUMN_INTERMEDIATEPARTYNAME + " TEXT," +
                        TRANSACTIONS_COLUMN_OPPOSINGPARTYBIC + " TEXT," +
                        TRANSACTIONS_COLUMN_CODE + " TEXT," +
                        TRANSACTIONS_COLUMN_BATCHID + " TEXT," +
                        TRANSACTIONS_COLUMN_TRANSACTIONREFERENCE + " TEXT," +
                        TRANSACTIONS_COLUMN_AUTHORIZATIONIDENTIFIER + " TEXT," +
                        TRANSACTIONS_COLUMN_CREDITORID + " TEXT," +
                        TRANSACTIONS_COLUMN_PAYMENTREFERENCE + " TEXT," +
                        TRANSACTIONS_COLUMN_DESCRIPTION1 + " TEXT," +
                        TRANSACTIONS_COLUMN_DESCRIPTION2 + " TEXT," +
                        TRANSACTIONS_COLUMN_DESCRIPTION3 + " TEXT," +
                        TRANSACTIONS_COLUMN_REASONRETURN + " TEXT," +
                        TRANSACTIONS_COLUMN_ORIGINALAMOUNT + " TEXT," +
                        TRANSACTIONS_COLUMN_ORIGINALCURRENCY + " TEXT," +
                        TRANSACTIONS_COLUMN_COURSE + " TEXT" +
                        ")"
        );

        db.execSQL(
                "CREATE TABLE " + LABELS_TABLE_NAME + "(" +
                        LABELS_COLUMN_ID + " INTEGER PRIMARY KEY," +
                        LABELS_COLUMN_NAME + " TEXT," +
                        LABELS_COLUMN_COLOR + " TEXT" +
                        ")"
        );

        db.execSQL(
                "CREATE TABLE " + LABELED_TRANSACTIONS_TABLE_NAME + "(" +
                        LABELED_TRANSACTIONS_COLUMN_TRANSACTION_ID + " INTEGER PRIMARY KEY," +
                        LABELED_TRANSACTIONS_COLUMN_LABEL_ID + " INTEGER PRIMARY KEY," +
                        "FOREIGN KEY (" + LABELED_TRANSACTIONS_COLUMN_TRANSACTION_ID + ") REFERENCES " + TRANSACTIONS_TABLE_NAME + "(" + TRANSACTIONS_COLUMN_ID + ")," +
                        "FOREIGN KEY (" + LABELED_TRANSACTIONS_COLUMN_LABEL_ID + ") REFERENCES " + LABELS_TABLE_NAME + "(" + LABELS_COLUMN_ID + ")" +
                        ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TRANSACTIONS_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertTransaction(StringTransaction stringTransaction) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSACTIONS_COLUMN_IBANBBAN, stringTransaction.ibanBban);
        contentValues.put(TRANSACTIONS_COLUMN_CURRENCY, stringTransaction.currency);
        contentValues.put(TRANSACTIONS_COLUMN_BIC, stringTransaction.bic);
        contentValues.put(TRANSACTIONS_COLUMN_FOLLOWNR, stringTransaction.followNr);
        contentValues.put(TRANSACTIONS_COLUMN_DATE, stringTransaction.date);
        contentValues.put(TRANSACTIONS_COLUMN_INTERESTDATE, stringTransaction.interestDate);
        contentValues.put(TRANSACTIONS_COLUMN_AMOUNT, stringTransaction.amount);
        contentValues.put(TRANSACTIONS_COLUMN_BALANCEAFTERTRN, stringTransaction.balanceAfterTrn);
        contentValues.put(TRANSACTIONS_COLUMN_OPPOSINGPARTYIBANBBAN, stringTransaction.opposingPartyIbanBbaN);
        contentValues.put(TRANSACTIONS_COLUMN_OPPOSINGPARTYNAME, stringTransaction.opposingPartyName);
        contentValues.put(TRANSACTIONS_COLUMN_EVENTUALPARTYNAME, stringTransaction.eventualPartyName);
        contentValues.put(TRANSACTIONS_COLUMN_INTERMEDIATEPARTYNAME, stringTransaction.intermediatePartyName);
        contentValues.put(TRANSACTIONS_COLUMN_OPPOSINGPARTYBIC, stringTransaction.opposingPartyBic);
        contentValues.put(TRANSACTIONS_COLUMN_CODE, stringTransaction.code);
        contentValues.put(TRANSACTIONS_COLUMN_BATCHID, stringTransaction.batchId);
        contentValues.put(TRANSACTIONS_COLUMN_TRANSACTIONREFERENCE, stringTransaction.transactionReference);
        contentValues.put(TRANSACTIONS_COLUMN_AUTHORIZATIONIDENTIFIER, stringTransaction.authorizationIdentifier);
        contentValues.put(TRANSACTIONS_COLUMN_CREDITORID, stringTransaction.creditorId);
        contentValues.put(TRANSACTIONS_COLUMN_PAYMENTREFERENCE, stringTransaction.paymentReference);
        contentValues.put(TRANSACTIONS_COLUMN_DESCRIPTION1, stringTransaction.description1);
        contentValues.put(TRANSACTIONS_COLUMN_DESCRIPTION2, stringTransaction.description2);
        contentValues.put(TRANSACTIONS_COLUMN_DESCRIPTION3, stringTransaction.description3);
        contentValues.put(TRANSACTIONS_COLUMN_REASONRETURN, stringTransaction.reasonReturn);
        contentValues.put(TRANSACTIONS_COLUMN_ORIGINALAMOUNT, stringTransaction.originalAmount);
        contentValues.put(TRANSACTIONS_COLUMN_ORIGINALCURRENCY,stringTransaction.originalCurrency);
        contentValues.put(TRANSACTIONS_COLUMN_COURSE, stringTransaction.course);

        db.insert(TRANSACTIONS_TABLE_NAME, null, contentValues);
        return true;
    }
}
