package com.example.mydndapplication.account_analyser;

public class StringTransaction {
    public String ibanBban;                   // "NL29RABO0111095379"
    public String currency;                   // "EUR"
    public String bic;                        // "RABONL2U"
    public String followNr;                   // "000000000000004243"
    public String date;                       // "2023-01-01"
    public String interestDate;               // "2023-01-01"
    public String amount;                     // "-44,35"
    public String balanceAfterTrn;            // "+610,53"
    public String opposingPartyIbanBbaN;      // "NL35RABO0117713678"
    public String opposingPartyName;          // "Grillroom Sahara via PAY.nl"
    public String eventualPartyName;          // ""
    public String intermediatePartyName;      // ""
    public String opposingPartyBic;           // "RABONL2U"
    public String code;                       // "id"
    public String batchId;                    // ""
    public String transactionReference;       // "01-01-2023 17:19 0370589954576968"
    public String authorizationIdentifier;    // ""
    public String creditorId;                 // ""
    public String paymentReference;           // ""
    public String description1;               // "1961107206X69e4d 0370589954576968 867-12776-Grillroom Sahara x5379 pasnr.044"
    public String description2;               // " "
    public String description3;               // ""
    public String reasonReturn;               // ""
    public String originalAmount;             // ""
    public String originalCurrency;           // ""
    public String course;                     // ""

    public static final class StringTransactionBuilder {
        private String ibanBban;
        private String currency;
        private String bic;
        private String followNr;
        private String date;
        private String interestDate;
        private String amount;
        private String balanceAfterTrn;
        private String opposingPartyIbanBbaN;
        private String opposingPartyName;
        private String eventualPartyName;
        private String intermediatePartyName;
        private String opposingPartyBic;
        private String code;
        private String batchId;
        private String transactionReference;
        private String authorizationIdentifier;
        private String creditorId;
        private String paymentReference;
        private String description1;
        private String description2;
        private String description3;
        private String reasonReturn;
        private String originalAmount;
        private String originalCurrency;
        private String course;

        public StringTransactionBuilder() {
        }

        public static StringTransactionBuilder aStringTransaction() {
            return new StringTransactionBuilder();
        }

        public StringTransactionBuilder withIbanBban(String ibanBban) {
            this.ibanBban = ibanBban;
            return this;
        }

        public StringTransactionBuilder withCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public StringTransactionBuilder withBic(String bic) {
            this.bic = bic;
            return this;
        }

        public StringTransactionBuilder withFollowNr(String followNr) {
            this.followNr = followNr;
            return this;
        }

        public StringTransactionBuilder withDate(String date) {
            this.date = date;
            return this;
        }

        public StringTransactionBuilder withInterestDate(String interestDate) {
            this.interestDate = interestDate;
            return this;
        }

        public StringTransactionBuilder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public StringTransactionBuilder withBalanceAfterTrn(String balanceAfterTrn) {
            this.balanceAfterTrn = balanceAfterTrn;
            return this;
        }

        public StringTransactionBuilder withOpposingPartyIbanBbaN(String opposingPartyIbanBbaN) {
            this.opposingPartyIbanBbaN = opposingPartyIbanBbaN;
            return this;
        }

        public StringTransactionBuilder withOpposingPartyName(String opposingPartyName) {
            this.opposingPartyName = opposingPartyName;
            return this;
        }

        public StringTransactionBuilder withEventualPartyName(String eventualPartyName) {
            this.eventualPartyName = eventualPartyName;
            return this;
        }

        public StringTransactionBuilder withIntermediatePartyName(String intermediatePartyName) {
            this.intermediatePartyName = intermediatePartyName;
            return this;
        }

        public StringTransactionBuilder withOpposingPartyBic(String opposingPartyBic) {
            this.opposingPartyBic = opposingPartyBic;
            return this;
        }

        public StringTransactionBuilder withCode(String code) {
            this.code = code;
            return this;
        }

        public StringTransactionBuilder withBatchId(String batchId) {
            this.batchId = batchId;
            return this;
        }

        public StringTransactionBuilder withTransactionReference(String transactionReference) {
            this.transactionReference = transactionReference;
            return this;
        }

        public StringTransactionBuilder withAuthorizationIdentifier(String authorizationIdentifier) {
            this.authorizationIdentifier = authorizationIdentifier;
            return this;
        }

        public StringTransactionBuilder withCreditorId(String creditorId) {
            this.creditorId = creditorId;
            return this;
        }

        public StringTransactionBuilder withPaymentReference(String paymentReference) {
            this.paymentReference = paymentReference;
            return this;
        }

        public StringTransactionBuilder withDescription1(String description1) {
            this.description1 = description1;
            return this;
        }

        public StringTransactionBuilder withDescription2(String description2) {
            this.description2 = description2;
            return this;
        }

        public StringTransactionBuilder withDescription3(String description3) {
            this.description3 = description3;
            return this;
        }

        public StringTransactionBuilder withReasonReturn(String reasonReturn) {
            this.reasonReturn = reasonReturn;
            return this;
        }

        public StringTransactionBuilder withOriginalAmount(String originalAmount) {
            this.originalAmount = originalAmount;
            return this;
        }

        public StringTransactionBuilder withOriginalCurrency(String originalCurrency) {
            this.originalCurrency = originalCurrency;
            return this;
        }

        public StringTransactionBuilder withCourse(String course) {
            this.course = course;
            return this;
        }

        public StringTransaction build() {
            StringTransaction stringTransaction = new StringTransaction();
            stringTransaction.authorizationIdentifier = this.authorizationIdentifier;
            stringTransaction.eventualPartyName = this.eventualPartyName;
            stringTransaction.balanceAfterTrn = this.balanceAfterTrn;
            stringTransaction.opposingPartyIbanBbaN = this.opposingPartyIbanBbaN;
            stringTransaction.paymentReference = this.paymentReference;
            stringTransaction.amount = this.amount;
            stringTransaction.intermediatePartyName = this.intermediatePartyName;
            stringTransaction.batchId = this.batchId;
            stringTransaction.interestDate = this.interestDate;
            stringTransaction.ibanBban = this.ibanBban;
            stringTransaction.opposingPartyName = this.opposingPartyName;
            stringTransaction.followNr = this.followNr;
            stringTransaction.code = this.code;
            stringTransaction.transactionReference = this.transactionReference;
            stringTransaction.bic = this.bic;
            stringTransaction.opposingPartyBic = this.opposingPartyBic;
            stringTransaction.originalCurrency = this.originalCurrency;
            stringTransaction.course = this.course;
            stringTransaction.currency = this.currency;
            stringTransaction.description1 = this.description1;
            stringTransaction.creditorId = this.creditorId;
            stringTransaction.reasonReturn = this.reasonReturn;
            stringTransaction.originalAmount = this.originalAmount;
            stringTransaction.date = this.date;
            stringTransaction.description2 = this.description2;
            stringTransaction.description3 = this.description3;
            return stringTransaction;
        }
    }
}
