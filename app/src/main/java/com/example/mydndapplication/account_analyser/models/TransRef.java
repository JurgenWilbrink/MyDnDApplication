package com.example.mydndapplication.account_analyser.models;

import com.example.mydndapplication.account_analyser.Transaction;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.regex.Pattern;

public class TransRef {

    String initialTransactionReference;  // "01-01-2023 17:19 0370589954576968"
    LocalDateTime dateTime;
    String reference;

    public TransRef(String initialTransactionReference) {
        this.initialTransactionReference = initialTransactionReference;
        String[] transRefSplit = initialTransactionReference.split(" ");

        LocalDate localDate = LocalDate.parse(Pattern.compile(Transaction.dateRegex)
                .matcher(transRefSplit[0]).group(1));
        LocalTime localTime = LocalTime.parse(Pattern.compile(Transaction.timeRegex)
                .matcher(transRefSplit[1]).group(1));
        this.dateTime = LocalDateTime.of(localDate, localTime);

        this.reference = transRefSplit[2];
    }

    public String getInitialTransactionReference() {
        return initialTransactionReference;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getReference() {
        return reference;
    }
}
