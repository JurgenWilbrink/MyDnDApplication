package com.example.mydndapplication.account_analyser.models;

public class IbanBban {

    String ibanString;   //"NL29RABO0111095379"
    Banks bank;          //"RABO"

    public IbanBban(String ibanString) {
        this.ibanString = ibanString;
        this.bank = Banks.valueOf(ibanString.substring(4, 7));
    }

    public String getIbanString() {
        return ibanString;
    }

    public Banks getBank() {
        return bank;
    }
}
