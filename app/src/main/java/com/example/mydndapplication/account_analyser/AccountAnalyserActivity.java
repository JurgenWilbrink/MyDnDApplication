package com.example.mydndapplication.account_analyser;

import static com.example.mydndapplication.Constants.PICKFILE_RESULT_CODE;
import static com.example.mydndapplication.Constants.PICK_FROM_GALLERY;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.manager_activities.TransactionAdapter;
import com.example.mydndapplication.model.DataProvider;

import java.util.List;

public class AccountAnalyserActivity extends AppCompatActivity {

    public static final String CID = "transactionID";
    public static int amountOfTransactions;

    private TextView addFileText;
    private ImageView arrowImage;

    private TransactionAdapter transactionAdapter;

    private List<Transaction> accountMonthItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list_view);

        accountMonthItemList = AAUtils.getTransactionList();

        amountOfTransactions = accountMonthItemList != null ? accountMonthItemList.size() : 0;

        //Log.i(SV.TAG_JSON, "onCreate: list on creation of listview " + campaignItemList);

        //Floating action button stuf
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        //clicklistener for the floating action button
        fab.setOnClickListener(view -> {
            try {
                if (ActivityCompat.checkSelfPermission(AccountAnalyserActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AccountAnalyserActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                    chooseFile.setType("text/plain");
                    startActivityForResult(
                            Intent.createChooser(chooseFile, "Choose a file"),
                            PICKFILE_RESULT_CODE
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // setting campaignAdapter
        transactionAdapter = new TransactionAdapter(this, accountMonthItemList);
        ListView listView = findViewById(R.id.transactionList);
        listView.setAdapter(transactionAdapter);

        // getting fields
        addFileText = findViewById(R.id.addTransaction);
        arrowImage = findViewById(R.id.arrowView);

        // by standard these will be set to Invisible
        addFileText.setVisibility(View.INVISIBLE);
        arrowImage.setVisibility(View.INVISIBLE);

        // if there are zero campaigns those will be set to visible, impaling that the using should add a campaign
//        if (DataProvider.campaignItemList.size() < 2){
//            addFileText.setVisibility(View.VISIBLE);
//            arrowImage.setVisibility(View.VISIBLE);
//        }

//        // initializing click listener
//        listView.setOnItemClickListener((parent, view, position, id) -> {
//           // Log.i("Click", "onCreate: clicked");
//            Intent intent = new Intent(AccountAnalyserActivity.this, CampaignDetailsActivity.class);
//            intent.putExtra(CID, DataProvider.getIdByPosition(position));
//            AccountAnalyserActivity.this.startActivity(intent);
//        });

//        // this is for the back button in the top bar
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

//        //reseting amount of transactions in case one has been deleted
//        amountOfTransactions =
//
//        // sorting list again
//        Collections.sort(AccountMonthItemList, (o1, o2) -> o1.getName().compareTo(o2.getName()));
//
//        // informing user that the list has been sorted
//        Toast.makeText(this, "List Sorted!", Toast.LENGTH_SHORT).show();
//
//        //campaignAdapter.notifyDataSetChanged();
//
//        //checking if the Arrow pointing at the "+" button should be there.
        if (amountOfTransactions < 2){
            addFileText.setVisibility(View.VISIBLE);
            arrowImage.setVisibility(View.VISIBLE);
        } else {
            addFileText.setVisibility(View.INVISIBLE);
            arrowImage.setVisibility(View.INVISIBLE);
        }
    }
}
