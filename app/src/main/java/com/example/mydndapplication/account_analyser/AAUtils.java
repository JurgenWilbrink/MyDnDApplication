package com.example.mydndapplication.account_analyser;

import static com.example.mydndapplication.account_analyser.Transaction.TransactionBuilder.aTransaction;

import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AAUtils {

    public static List<List<String>> getCSV(String filePath) throws FileNotFoundException {

        filePath = "com/example/mydndapplication/init_files/CSV_A_NL29RABO0111095379_EUR_202301.csv";

        List<List<String>> records = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(new FileReader(filePath));) {
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    public static List<Transaction> getTransactionList() {
        try {
            List<Transaction> transactionList = new ArrayList<>();
            List<List<String>> csvRecords = getCSV("blu");
            for (List<String> record : csvRecords) {
                transactionList.add(createTransaction(record));
            }
            return transactionList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<StringTransaction> getStringTransactionList() {
        try {
            List<StringTransaction> stringTransactions = new ArrayList<>();
            List<List<String>> csvRecords = getCSV("blu");
            for (List<String> record : csvRecords) {
                stringTransactions.add(createStringTransaction(record));
            }
            return stringTransactions;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Transaction createTransaction(List<String> attributes) {
        return new Transaction.TransactionBuilder()
                .withIbanBban(attributes.get(0))
                .withCurrency(attributes.get(1))
                .withBic(attributes.get(2))
                .withFollowNr(attributes.get(3))
                .withDate(attributes.get(4))
                .withInterestDate(attributes.get(5))
                .withAmount(attributes.get(6))
                .withBalanceAfterTrn(attributes.get(7))
                .withOpposingPartyIbanBbaN(attributes.get(8))
                .withOpposingPartyName(attributes.get(9))
                .withEventualPartyName(attributes.get(10))
                .withIntermediatePartyName(attributes.get(11))
                .withOpposingPartyBic(attributes.get(12))
                .withCode(attributes.get(13))
                .withBatchId(attributes.get(14))
                .withTransactionReference(attributes.get(15))
                .withAuthorizationIdentifier(attributes.get(16))
                .withCreditorId(attributes.get(17))
                .withPaymentReference(attributes.get(18))
                .withDescription1(attributes.get(19))
                .withDescription2(attributes.get(20))
                .withDescription3(attributes.get(21))
                .withReasonReturn(attributes.get(22))
                .withOriginalAmount(attributes.get(23))
                .withOriginalCurrency(attributes.get(24))
                .withCourse(attributes.get(25))
                .build();
    }

    public static StringTransaction createStringTransaction(List<String> attributes) {
        return new StringTransaction.StringTransactionBuilder()
                .withIbanBban(attributes.get(0))
                .withCurrency(attributes.get(1))
                .withBic(attributes.get(2))
                .withFollowNr(attributes.get(3))
                .withDate(attributes.get(4))
                .withInterestDate(attributes.get(5))
                .withAmount(attributes.get(6))
                .withBalanceAfterTrn(attributes.get(7))
                .withOpposingPartyIbanBbaN(attributes.get(8))
                .withOpposingPartyName(attributes.get(9))
                .withEventualPartyName(attributes.get(10))
                .withIntermediatePartyName(attributes.get(11))
                .withOpposingPartyBic(attributes.get(12))
                .withCode(attributes.get(13))
                .withBatchId(attributes.get(14))
                .withTransactionReference(attributes.get(15))
                .withAuthorizationIdentifier(attributes.get(16))
                .withCreditorId(attributes.get(17))
                .withPaymentReference(attributes.get(18))
                .withDescription1(attributes.get(19))
                .withDescription2(attributes.get(20))
                .withDescription3(attributes.get(21))
                .withReasonReturn(attributes.get(22))
                .withOriginalAmount(attributes.get(23))
                .withOriginalCurrency(attributes.get(24))
                .withCourse(attributes.get(25))
                .build();
    }
}
