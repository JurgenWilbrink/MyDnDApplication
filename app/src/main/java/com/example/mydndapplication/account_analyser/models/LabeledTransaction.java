package com.example.mydndapplication.account_analyser.models;

import com.example.mydndapplication.account_analyser.Transaction;

import java.util.ArrayList;
import java.util.List;

public class LabeledTransaction {
    Transaction transaction;
    List<Label> labels;

    public LabeledTransaction(Transaction transaction) {
        this.transaction = transaction;
        this.labels = new ArrayList<>();
    }

    public void addLabel(Label label) {
        if (!labels.contains(label)){
            labels.add(label);
        } else {
            //Send Already exists Error
        }
    }
}
