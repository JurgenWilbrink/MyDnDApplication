package com.example.mydndapplication.account_analyser.models;

import android.graphics.Color;

public class Label {
    String name;
    Color color;

    public Label(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
