package com.example.mydndapplication.account_analyser.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;

public class DBCursorDriver implements SQLiteCursorDriver {

    @Override
    public Cursor query(SQLiteDatabase.CursorFactory cursorFactory, String[] strings) {
        return null;
    }

    @Override
    public void cursorDeactivated() {
        //NOOP
    }

    @Override
    public void cursorRequeried(Cursor cursor) {
        //NOOP
    }

    @Override
    public void cursorClosed() {
        //NOOP
    }

    @Override
    public void setBindArguments(String[] strings) {
        //NOOP
    }
}
