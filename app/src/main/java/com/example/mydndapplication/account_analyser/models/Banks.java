package com.example.mydndapplication.account_analyser.models;

import java.util.Objects;

public enum Banks {

    ABN_AMRO("ABN Ambro", "ABNANL2A", "ABNA"),
    ASN_BANK("ANS Bank", "ASNBNL21", "ASNB"),
    ING_BANK("ING Bank", "INGBNL2A", "INGB"),
    KNAB("KNAB", "KNABNL2H", "KNAB"),
    MONEYOU("MONEY YOU", "MOYONL21", "MOYO"),
    NATIONALE_NEDERLANDEN_BANK("Nationale Nederlanden Bank", "NNBANL2G", "NNBA"),
    RABOBANK("Rabobank", "RABONL2U", "RABO"),
    REGIOBANK("Regiobank", "RBRBNL21", "RBRB"),
    SNS_BANK("SNS Bank", "SNSBNL2A", "SNSB"),
    TRIODOS_BANK("Triodos Bank", "TRIONL2U", "TRIO"),
    VAN_LANSCHOT_KEMPEN("Van Landschot Kempen", "FVLBNL22", "FVLB");

    private final String name;
    private final String bic;
    private final String fourShort;

    Banks(String name, String bic, String fourShort){
        this.name = name;
        this.bic = bic;
        this.fourShort = fourShort;
    }

    public static Banks bankByBic(String bic) {
        for (Banks b : Banks.values()) {
            if (Objects.equals(b.bic, bic)) {
                return b;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public String getFourShort() {
        return fourShort;
    }

    public String getBic() {
        return bic;
    }
}
