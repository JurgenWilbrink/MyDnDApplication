package com.example.mydndapplication.account_analyser.database;

import static android.database.sqlite.SQLiteDatabase.findEditTable;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;
import static android.database.sqlite.SQLiteDatabase.CursorFactory;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteQueryBuilder;

import com.example.mydndapplication.account_analyser.StringTransaction;
// new SQLiteQueryBuilder()

public class Database {

    private static CursorFactory cursorFactory;
    private static DBCursorDriver cursorDriver;

    private static DBHelper dbHelper;

    //private SQLiteDatabase myDataBase = openOrCreateDatabase("MyDatabase", ,null);

    public void init() {
        cursorFactory = new DBCursorFactory();
        cursorDriver = new DBCursorDriver();

        dbHelper = new DBHelper(null, DBHelper.DATABASE_NAME, cursorFactory, 1);
    }

    public static void addTransaction(StringTransaction stringTransaction) {
        dbHelper.insertTransaction(stringTransaction);
    }
}
