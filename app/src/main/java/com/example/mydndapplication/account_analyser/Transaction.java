package com.example.mydndapplication.account_analyser;

import com.example.mydndapplication.account_analyser.models.Banks;
import com.example.mydndapplication.account_analyser.models.IbanBban;
import com.example.mydndapplication.account_analyser.models.MyCurrency;
import com.example.mydndapplication.account_analyser.models.TransRef;

import java.time.LocalDate;
import java.util.Currency;
import java.util.regex.Pattern;

// Idea for logbook per account? / per person? // being able to group them yourself? // by making 'a new person' ?
// a set of standard labels?
// a label can be a person?


public class Transaction {

    public static String dateRegex = "(\\d{4}-\\d{2}-\\d{2})";
    public static String timeRegex = "(\\d{2}:\\d{2})";

    // Parameters
    public String ibanBban;                // "NL29RABO0111095379"                // -
    public MyCurrency currency;            // "EUR"                               // munt
    public Banks bic;                      // "RABONL2U"                          // -
    public String followNr;                // "000000000000004243"                // volgnr
    public LocalDate date;                 // "2023-01-01"                        // datum
    public LocalDate interestDate;          // "2023-01-01"                        // rentedatum
    public Double amount;                  // "-44,35"                            // bedrag
    public Double balanceAfterTrn;         // "+610,53"                           // saldoNaTrn
    public String opposingPartyIbanBbaN;   // "NL35RABO0117713678"                // tegenrekeningIbanBbaN
    public String opposingPartyName;       // "Grillroom Sahara via PAY.nl"       // naamTegenpartij
    public String eventualPartyName;       // ""                                  // naamUiteindelijkePartij
    public String intermediatePartyName;   // ""                                  // naamInitierendePartij
    public Banks opposingPartyBic;         // "RABONL2U"                          // bicTegenpartij
    public String code;                    // "id"                                // -
    public String batchId;                 // ""                                  // -
    public TransRef transactionReference;  // "01-01-2023 17:19 0370589954576968" // transactiereferentie
    public String authorizationIdentifier; // ""                                  // machtigingskenmerk
    public String creditorId;              // ""                                  // incassantId
    public String paymentReference;        // ""                                  // betalingskenmerk
    public String description1;            // "1961107206X69e4d 0370589954576968 867-12776-Grillroom Sahara x5379 pasnr.044"  // beschrijving1
    public String description2;            // " "                                 // omschrijving2
    public String description3;            // ""                                  // omschrijving3
    public String reasonReturn;            // ""                                  // redenRetour
    public String originalAmount;          // ""                                  // oorsprBedrag
    public MyCurrency originalCurrency;    // ""                                  // oorsprMunt
    public String course;                  // ""                                  // koers

    public static final class TransactionBuilder {
        private String ibanBban;
        private MyCurrency currency;
        private Banks bic;
        private String followNr;
        private LocalDate date;
        private LocalDate interestDate;
        private Double amount;
        private Double balanceAfterTrn;
        private String opposingPartyIbanBbaN;
        private String opposingPartyName;
        private String eventualPartyName;
        private String intermediatePartyName;
        private Banks opposingPartyBic;
        private String code;
        private String batchId;
        private TransRef transactionReference;
        private String authorizationIdentifier;
        private String creditorId;
        private String paymentReference;
        private String description1;
        private String description2;
        private String description3;
        private String reasonReturn;
        private String originalAmount;
        private MyCurrency originalCurrency;
        private String course;

        public TransactionBuilder() {
        }

        public static TransactionBuilder aTransaction() {
            return new TransactionBuilder();
        }

        public TransactionBuilder withIbanBban(String ibanBban) {
            this.ibanBban = ibanBban;
            return this;
        }

        public TransactionBuilder withCurrency(String currency) {
            this.currency = MyCurrency.fromIso(currency);
            return this;
        }

        public TransactionBuilder withBic(String bic) {
            this.bic = Banks.bankByBic(bic);
            return this;
        }

        public TransactionBuilder withFollowNr(String followNr) {
            this.followNr = followNr;
            return this;
        }

        public TransactionBuilder withDate(String date) {
            this.date = LocalDate.parse(Pattern.compile(dateRegex).matcher(date).group(1));
            return this;
        }

        public TransactionBuilder withInterestDate(String interestDate) {
            this.interestDate = LocalDate.parse(Pattern.compile(dateRegex).matcher(interestDate).group(1));
            return this;
        }

        public TransactionBuilder withAmount(String amount) {
            this.amount = Double.valueOf(amount);
            return this;
        }

        public TransactionBuilder withBalanceAfterTrn(String balanceAfterTrn) {
            this.balanceAfterTrn = Double.valueOf(balanceAfterTrn);
            return this;
        }

        public TransactionBuilder withOpposingPartyIbanBbaN(String opposingPartyIbanBbaN) {
            this.opposingPartyIbanBbaN = opposingPartyIbanBbaN;
            return this;
        }

        public TransactionBuilder withOpposingPartyName(String opposingPartyName) {
            this.opposingPartyName = opposingPartyName;
            return this;
        }

        public TransactionBuilder withEventualPartyName(String eventualPartyName) {
            this.eventualPartyName = eventualPartyName;
            return this;
        }

        public TransactionBuilder withIntermediatePartyName(String intermediatePartyName) {
            this.intermediatePartyName = intermediatePartyName;
            return this;
        }

        public TransactionBuilder withOpposingPartyBic(String opposingPartyBic) {
            this.opposingPartyBic = Banks.bankByBic(opposingPartyBic);
            return this;
        }

        public TransactionBuilder withCode(String code) {
            this.code = code;
            return this;
        }

        public TransactionBuilder withBatchId(String batchId) {
            this.batchId = batchId;
            return this;
        }

        public TransactionBuilder withTransactionReference(String transactionReference) {
            this.transactionReference = new TransRef(transactionReference);
            return this;
        }

        public TransactionBuilder withAuthorizationIdentifier(String authorizationIdentifier) {
            this.authorizationIdentifier = authorizationIdentifier;
            return this;
        }

        public TransactionBuilder withCreditorId(String creditorId) {
            this.creditorId = creditorId;
            return this;
        }

        public TransactionBuilder withPaymentReference(String paymentReference) {
            this.paymentReference = paymentReference;
            return this;
        }

        public TransactionBuilder withDescription1(String description1) {
            this.description1 = description1;
            return this;
        }

        public TransactionBuilder withDescription2(String description2) {
            this.description2 = description2;
            return this;
        }

        public TransactionBuilder withDescription3(String description3) {
            this.description3 = description3;
            return this;
        }

        public TransactionBuilder withReasonReturn(String reasonReturn) {
            this.reasonReturn = reasonReturn;
            return this;
        }

        public TransactionBuilder withOriginalAmount(String originalAmount) {
            this.originalAmount = originalAmount;
            return this;
        }

        public TransactionBuilder withOriginalCurrency(String originalCurrency) {
            this.originalCurrency = MyCurrency.fromIso(originalCurrency);
            return this;
        }

        public TransactionBuilder withCourse(String course) {
            this.course = course;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            transaction.ibanBban = this.ibanBban;
            transaction.currency = this.currency;
            transaction.bic = this.bic;
            transaction.followNr = this.followNr;
            transaction.date = this.date;
            transaction.interestDate = this.interestDate;
            transaction.amount = this.amount;
            transaction.balanceAfterTrn = this.balanceAfterTrn;
            transaction.opposingPartyIbanBbaN = this.opposingPartyIbanBbaN;
            transaction.opposingPartyName = this.opposingPartyName;
            transaction.eventualPartyName = this.eventualPartyName;
            transaction.intermediatePartyName = this.intermediatePartyName;
            transaction.opposingPartyBic = this.opposingPartyBic;
            transaction.code = this.code;
            transaction.batchId = this.batchId;
            transaction.transactionReference = this.transactionReference;
            transaction.authorizationIdentifier = this.authorizationIdentifier;
            transaction.creditorId = this.creditorId;
            transaction.paymentReference = this.paymentReference;
            transaction.description1 = this.description1;
            transaction.description2 = this.description2;
            transaction.description3 = this.description3;
            transaction.reasonReturn = this.reasonReturn;
            transaction.originalAmount = this.originalAmount;
            transaction.originalCurrency = this.originalCurrency;
            transaction.course = this.course;
            return transaction;
        }
    }
}
