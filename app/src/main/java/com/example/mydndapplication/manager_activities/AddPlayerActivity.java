package com.example.mydndapplication.manager_activities;

import static com.example.mydndapplication.Constants.PICK_FROM_GALLERY;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mydndapplication.R;
import com.example.mydndapplication.custom_views.GeneralPlayerInformationView;
import com.example.mydndapplication.model.Player;
import com.example.mydndapplication.model.DataProvider;

public class AddPlayerActivity extends AppCompatActivity {

    private ImageView playerImage;

    private GeneralPlayerInformationView generalPlayerInfoView;

    private String actualCustomImage = "";

    private int campaignId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player);

        // getting right campaignItem
        campaignId = getIntent().getIntExtra(CampaignListViewActivity.CID,-1);

        // finding views
        generalPlayerInfoView = findViewById(R.id.playerAttributeFieldsView);
        playerImage = findViewById(R.id.imageChangerPlayerImage);

        // setting empty player
        // (Actually not needed, in this case only sets the value of the HP field to 1 so it can not be set lower)
        generalPlayerInfoView.setGeneralPlayerInfo(DataProvider.emptyPlayerTemplate);

        playerImage.setOnClickListener(view -> {
            try {
                if (ActivityCompat.checkSelfPermission(AddPlayerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddPlayerActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void cancelAddPlayer(View view) {
        // finishing activity without adding the new player
        finish();
    }

    public void confirmAddPlayer(View view) {

        //getting player item from custom view
        Player tempPlayer = generalPlayerInfoView.getFieldInputAsPlayerItem();

        // checking if fields are empty
        if (tempPlayer.getPlayerName().isEmpty() ||
                tempPlayer.getCharacterName().isEmpty() ||
                String.valueOf(tempPlayer.getCharacterHP()).isEmpty() ||
                tempPlayer.getCharacterClass().isEmpty() ||
                tempPlayer.getCharacterWeapon().isEmpty()) {
            Toast.makeText(this, "A Field is Empty!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("ConfirmCheck", "runBack: all fields are NOT empty ");

            String whatImageHere;
            boolean customImageQ;

            // getting data from image
            if (actualCustomImage.isEmpty()) {
                whatImageHere = "nopp.jpg";
                customImageQ = false;
                Log.i("imageChecks", "confirmAddPlayer: " + whatImageHere);
            } else {
                whatImageHere = actualCustomImage;
                customImageQ = true;
                Log.i("imageChecks", "confirmAddPlayer: " + whatImageHere);
            }

            // adding player to current campaign

            DataProvider.findCampaignById(campaignId).addPlayerToList(new Player(
                    String.valueOf(tempPlayer.getPlayerName()),
                    String.valueOf(tempPlayer.getCharacterName()),
                    Integer.parseInt(String.valueOf(tempPlayer.getCharacterHP())),
                    String.valueOf(tempPlayer.getCharacterClass()),
                    String.valueOf(tempPlayer.getCharacterWeapon()),
                    whatImageHere,
                    customImageQ));

            Toast.makeText(this, "Added!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);

                actualCustomImage = cursor.getString(columnIndex);
                Log.i("imageChecks", "onActivityResult: image has been set to actualCustomImage");

                cursor.close();

                // Set the Image in ImageView after decoding the String
                playerImage.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
