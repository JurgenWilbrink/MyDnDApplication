package com.example.mydndapplication.manager_activities;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.mydndapplication.custom_views.CustomPlayerView;
import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.Player;
import com.example.mydndapplication.model.DataProvider;

import java.io.File;
import java.util.List;

public class PlayerAdapter extends ArrayAdapter<Player> {


    public PlayerAdapter(Context context, List<Player> objects) {
        super(context, R.layout.player_list_item, objects);
    }

    public View getView(int playerPosition, View convertView, ViewGroup parent) {

        // getting player and campaign ID
        int playerId = getItem(playerPosition).getPlayerNr();
        int campaignId = CampaignDetailsActivity.campaignId;

        CampaignItem campI = DataProvider.findCampaignById(campaignId);

        if (convertView == null) {
            convertView = new CustomPlayerView(getContext());
        }

        // getting player item
        Player item = campI.findPlayerById(playerId);

        //DeletionCheck
        File imgFile = new File(item.getImage());
        if (item.isCustomImage() && !(imgFile.exists())) {
            campI.findPlayerById(playerId).setImage("nopp.jpg");
            campI.findPlayerById(playerId).setCustomImage(false);
            DataProvider.setCampaignItem(campI, campaignId);
        }

        // setting custom player view -> compound control
        ((CustomPlayerView) convertView).setPlayer(item);

        return convertView;
    }
}
