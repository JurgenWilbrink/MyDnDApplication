package com.example.mydndapplication.manager_activities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.mydndapplication.custom_views.CustomCampaignView;
import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;

import java.io.File;
import java.util.List;

public class CampaignAdapter extends ArrayAdapter<CampaignItem> {

    public CampaignAdapter(Context context, List<CampaignItem> objects) {
        super(context, R.layout.campaign_list_item, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new CustomCampaignView(getContext());
        }

        CampaignItem item = getItem(position);
        int campaignId = item.getCampaignNr();

        //Deletion Check
        File imgFile = new File(item.getImage());
        if (item.isCustomImage() && !(imgFile.exists())) {
            item.setImage("noimage.jpg");
            item.setCustomImage(false);
            DataProvider.setCampaignItem(item, campaignId);
        }

        // Custom view -> Compound control
        ((CustomCampaignView) convertView).setCampaign(item);

        return convertView;
    }
}
