package com.example.mydndapplication.manager_activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class DrawingCharacterHP extends View {

    private Paint paintRed = new Paint();
    private int gettingChrHP = 0;

    public DrawingCharacterHP(Context context) {
        super(context);
        init();
    }

    public DrawingCharacterHP(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingCharacterHP(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DrawingCharacterHP(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        //setting base paint
        paintRed.setColor(Color.RED);
        paintRed.setStrokeWidth(4f);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //calculating some values
        float width = (float) (getWidth() / 10) * 9;
        float height = (float) (getHeight() / 10) * 9;
        float marge = width / 20;
        float aSixth = width / 6;
        float aHalf = width / 2;
        float aNTOTF = (width / 24) * 19; // a Nine Teen Of Twenty Four

        // initializing points of the Hexagon
        // Initialized as:
        //
        //      11  12      2   3
        //  10          1           4
        //
        //  9                       5
        //
        //      8              6
        //
        //              7
        //

        //setting X and Y's for the points relative to the matrix
        float[][] heartXYs = {
                {aHalf + marge, aSixth + marge},
                {aHalf + aSixth + marge, marge},
                {aHalf + (2 * aSixth) + marge, marge},
                {width + marge, aSixth + marge},
                {width + marge, aHalf + marge},
                {width - aSixth + marge, aNTOTF + marge},
                {aHalf + marge, width + marge},
                {aSixth + marge, aNTOTF+ marge},
                {marge, aHalf + marge},
                {marge, aSixth + marge},
                {aSixth + marge, marge},
                {(2 * aSixth) + marge, marge}
        };

        // drawing the lines between those points
        for (int i = 0; i < 11; i++) {
            canvas.drawLine(heartXYs[i][0], heartXYs[i][1], heartXYs[i + 1][0], heartXYs[i + 1][1], paintRed);
        }
        canvas.drawLine(heartXYs[11][0], heartXYs[11][1], heartXYs[0][0], heartXYs[0][1], paintRed);

        // setting paint text
        paintRed.setTextSize((width / 19) * 8);

        // getting the XY values for the Text so that it displays in the middle no matter the amount of characters in the text.
        float[] textXYs = {(width / 2) + marge, (height / 2) + marge - ((paintRed.descent() + paintRed.ascent()) / 2)};

        // draw the text
        paintRed.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(String.valueOf(gettingChrHP), textXYs[0], textXYs[1], paintRed);

        // invalidate so that it updates properly
        invalidate();
    }

    public void setGettingChrHP(int hp){
        this.gettingChrHP = hp;
    }
}

