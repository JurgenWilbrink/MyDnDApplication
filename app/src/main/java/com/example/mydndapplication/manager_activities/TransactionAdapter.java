package com.example.mydndapplication.manager_activities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.mydndapplication.R;
import com.example.mydndapplication.account_analyser.Transaction;
import com.example.mydndapplication.custom_views.CustomCampaignView;
import com.example.mydndapplication.custom_views.CustomTransactionView;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;

import java.io.File;
import java.util.List;

public class TransactionAdapter extends ArrayAdapter<Transaction> {

    public TransactionAdapter(Context context, List<Transaction> objects) {
        super(context, R.layout.transaction_list_item, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new CustomCampaignView(getContext());
        }

        Transaction item = getItem(position);

        // Custom view -> Compound control
        ((CustomTransactionView) convertView).setTransaction(item);

        return convertView;
    }
}
