package com.example.mydndapplication.manager_activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mydndapplication.R;
import com.example.mydndapplication.custom_views.GeneralCampaignAddChangeInfoView;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;

import java.util.ArrayList;

public class AddCampaignActivity extends AppCompatActivity {

    private ImageView campaignImage;

    private GeneralCampaignAddChangeInfoView generalCampaignInfoView;

    private String actualCustomImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_campaign);

        // finding views
        generalCampaignInfoView = findViewById(R.id.campaignAttributeFields);
        campaignImage = findViewById(R.id.imageChangerCampaignImage);

        // (Actually not needed, but for consistency reasons...)
        generalCampaignInfoView.setCampaignItemToFields(DataProvider.emptyCampaignTemplate);

        // getting image from storage code:
        // From: https://stackoverflow.com/questions/39866869/how-to-ask-permission-to-access-gallery-on-android-m/39866945#39866945
        final int PICK_FROM_GALLERY = 1;

        campaignImage.setOnClickListener(view -> {
            try {
                if (ActivityCompat.checkSelfPermission(AddCampaignActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AddCampaignActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void cancelAddCampaign(View view) {
        // finishing activity without adding the new campaign
        finish();
    }

    public void confirmAddCampaign(View view) {
        //getting Fields value in campaign Item Format
        CampaignItem tempCampaign = generalCampaignInfoView.getFieldInputAsCampaignItem();

        //checking if fields are empty
        if (tempCampaign.getName().isEmpty() || tempCampaign.getWorldName().isEmpty() || tempCampaign.getDungeonMaster().isEmpty() || tempCampaign.getDescription().isEmpty()) {
            Toast.makeText(this, "A Field is Empty!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("ConfirmCheck", "runBack: all fields are NOT empty ");

            String whatImageHere;
            boolean customImageQ;

            // getting data from image
            if (actualCustomImage.isEmpty()) {
                whatImageHere = "noimage.jpg";
                customImageQ = false;
                Log.i("imageChecks", "confirmAddCampaign: " + whatImageHere);
            } else {
                whatImageHere = actualCustomImage;
                customImageQ = true;
                Log.i("imageChecks", "confirmAddCampaign: " + whatImageHere);
            }

            //adding new campaignitem
            DataProvider.campaignItemList.add(new CampaignItem(DataProvider.emptyCampaignTemplate.getPlayerList(),
                    tempCampaign.getName(),
                    tempCampaign.getDescription(),
                    tempCampaign.getWorldName(),
                    whatImageHere,
                    tempCampaign.getDungeonMaster(),
                    customImageQ));

            Toast.makeText(this, "Added!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        // Result code is RESULT_OK only if the user selects an Image
        try {
            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);

                actualCustomImage = cursor.getString(columnIndex);
                Log.i("imageChecks", "onActivityResult: image has been set to actualCustomImage");

                cursor.close();

                // Set the Image in ImageView after decoding the String
                campaignImage.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
