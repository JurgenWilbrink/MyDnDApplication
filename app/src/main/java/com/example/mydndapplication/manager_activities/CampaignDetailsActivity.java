package com.example.mydndapplication.manager_activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mydndapplication.R;
import com.example.mydndapplication.custom_views.CampaignDetailsView;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;

public class CampaignDetailsActivity extends AppCompatActivity {

    public static String PID = "playerID";
    public static int campaignId;

    private TextView noPlayerMsg;
    private CampaignItem campI;

    private ListView playerListView;
    private CampaignDetailsView cdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign_details);

        // getting current campaign ID
        campaignId = getIntent().getIntExtra(CampaignListViewActivity.CID, -1);

        // getting campaign item with ID
        campI = DataProvider.findCampaignById(campaignId);

        // if campaign exists then start setting the fields with the campaign data
        if(DataProvider.doesCampaignExistById(campaignId)){

            cdView = findViewById(R.id.campaignDetailsView);
            cdView.setCampaignDetails(campaignId);

            noPlayerMsg = findViewById(R.id.noPlayersMsg);

            // initializing player adapter
            PlayerAdapter playerAdapter = new PlayerAdapter(this, campI.getPlayerList());

            // setting player adapter
            playerListView = findViewById(R.id.playerListView);
            playerListView.setAdapter(playerAdapter);

            // the listener for the player adapter
            playerListView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {
                Log.i("Click", "onCreate: clicked");
                Intent intent = new Intent(this, PlayerChangerActivity.class);
                intent.putExtra(CampaignListViewActivity.CID, campaignId);
                intent.putExtra(PID, campI.findIdByPosition(position));
                startActivity(intent);
            });

            // if there are no players in a campaign show a message which says that there are no players in the campaign yet.
            if (campI.getPlayerListSize() < 1) {
                noPlayerMsg.setVisibility(View.VISIBLE);
            }

        } else {
            Log.i("checks", "onCreate: skipped");
            finish();
        }

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void startCampaignChangeIntent(View view) {
        Intent intent = new Intent(this, CampaignChangerActivity.class);
        intent.putExtra(CampaignListViewActivity.CID, campaignId);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Clear", "onResume: OutIf");

        if (CampaignListViewActivity.amountOfCampaigns > DataProvider.campaignItemList.size()) {
            // if so, a campaign has been deleted. so this campaign viewer Activity should be finished.
            // Otherwise it will show the next campaign in line, in the campaign viewer activity, which is wrong
            // it SHOULD show the list of campaigns which it will do after implementation of this method
            Log.i("Clear", "onResume: InIf");
            finish();
            return;
        }

        //updating data from adapter
        ((BaseAdapter) playerListView.getAdapter()).notifyDataSetChanged();

        //updating data from details view
        cdView.setCampaignDetails(campaignId);

        if (campI.getPlayerListSize() < 1) {
            noPlayerMsg.setVisibility(View.VISIBLE);
        } else {
            noPlayerMsg.setVisibility(View.INVISIBLE);
        }
    }

    public void startAddPlayerIntent(View view) {
        // start add player activity
        Intent intent = new Intent(this, AddPlayerActivity.class);
        intent.putExtra(CampaignListViewActivity.CID, campaignId);
        startActivity(intent);
    }
}

