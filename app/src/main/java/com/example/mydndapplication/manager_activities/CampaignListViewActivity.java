package com.example.mydndapplication.manager_activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mydndapplication.R;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;
import com.example.mydndapplication.model.SV;

import java.util.Collections;
import java.util.List;

public class CampaignListViewActivity extends AppCompatActivity {

    public static final String CID = "campaignID";
    public static int amountOfCampaigns;

    private TextView addCampText;
    private ImageView arrowImage;

    private CampaignAdapter campaignAdapter;

    private List<CampaignItem> campaignItemList = DataProvider.campaignItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign_list_view);

        Log.i(SV.TAG_JSON, "onCreate: list on creation of listview " + campaignItemList);

        //Floating action button stuf
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        //clicklistener for the floating action button
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(CampaignListViewActivity.this, AddCampaignActivity.class);
            startActivity(intent);
        });

        // asking amount of campaigns
        amountOfCampaigns = DataProvider.campaignItemList.size();

        // setting campaignAdapter
        campaignAdapter = new CampaignAdapter(this, campaignItemList);
        ListView listView = findViewById(R.id.campaignList);
        listView.setAdapter(campaignAdapter);

        // getting fields
        addCampText = findViewById(R.id.addCampaigns);
        arrowImage = findViewById(R.id.arrowView);

        // by standard these will be set to Invisible
        addCampText.setVisibility(View.INVISIBLE);
        arrowImage.setVisibility(View.INVISIBLE);

        // if there are zero campaigns those will be set to visible, impaling that the using should add a campaign
        if (DataProvider.campaignItemList.size() < 2){
            addCampText.setVisibility(View.VISIBLE);
            arrowImage.setVisibility(View.VISIBLE);
        }

        // initializing click listener
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Log.i("Click", "onCreate: clicked");
            Intent intent = new Intent(CampaignListViewActivity.this, CampaignDetailsActivity.class);
            intent.putExtra(CID, DataProvider.getIdByPosition(position));
            CampaignListViewActivity.this.startActivity(intent);
        });

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //reseting amount of campaigns in case one has been deleted
        amountOfCampaigns = DataProvider.campaignItemList.size();

        // sorting list again
        Collections.sort(campaignItemList, (o1, o2) -> o1.getName().compareTo(o2.getName()));

        // informing user that the list has been sorted
        Toast.makeText(this, "List Sorted!", Toast.LENGTH_SHORT).show();

        campaignAdapter.notifyDataSetChanged();

        //checking if the Arrow pointing at the "+" button should be there.
        if (DataProvider.campaignItemList.size() < 2){
            addCampText.setVisibility(View.VISIBLE);
            arrowImage.setVisibility(View.VISIBLE);
        } else {
            addCampText.setVisibility(View.INVISIBLE);
            arrowImage.setVisibility(View.INVISIBLE);
        }
    }
}
