package com.example.mydndapplication.manager_activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mydndapplication.R;
import com.example.mydndapplication.custom_views.GeneralPlayerInformationView;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.Player;
import com.example.mydndapplication.model.DataProvider;
import com.example.mydndapplication.model.Util;

import java.io.File;

public class PlayerChangerActivity extends AppCompatActivity {

    private ImageView playerImage;

    private GeneralPlayerInformationView generalPlayerInfoView;

    private int campaignId;
    private int playerId;
    private String actualCustomPlayerImage = "";

    private CampaignItem currentCampaignItem;
    private Player currentPlayerItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_changer);

        // getting right campaignItem
        campaignId = getIntent().getIntExtra(CampaignListViewActivity.CID,-1);
        playerId = getIntent().getIntExtra(CampaignDetailsActivity.PID,-1);

        // getting right player item
        currentCampaignItem = DataProvider.findCampaignById(campaignId);
        currentPlayerItem = currentCampaignItem.findPlayerById(playerId);

        // finding views
        playerImage = findViewById(R.id.imageChangerPlayerImage);
        generalPlayerInfoView = findViewById(R.id.playerAttributeFieldsView);

        // Setting current values to edit Fields
        generalPlayerInfoView.setGeneralPlayerInfo(currentPlayerItem);


        //deletion check, if deleted, value reset.
        File imgFile = new File(currentPlayerItem.getImage());
        if (currentPlayerItem.isCustomImage() && !(imgFile.exists())) {
            currentPlayerItem.setImage("nopp.jpg");
            currentPlayerItem.setCustomImage(false);
            currentCampaignItem.setPlayerToList(currentPlayerItem, playerId);
            DataProvider.setCampaignItem(currentCampaignItem, campaignId);
        }

        // Setting Image
        Util.setImage(currentPlayerItem, playerImage, this);

        // with this the image can be clicked and a new image can be added from the gallery.
        final int PICK_FROM_GALLERY = 1;
        playerImage.setOnClickListener(view -> {
            try {
                if (ActivityCompat.checkSelfPermission(PlayerChangerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(PlayerChangerActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void cancelPlayerEdit(View view) {
        // finishing edit, without adjusting the changes
        finish();
    }

    public void confirmPlayerEdit(View view) {
        // getting values from fields in player format
        Player tempPlayer = generalPlayerInfoView.getFieldInputAsPlayerItem();

        //checking if one of the fields in empty
        if (tempPlayer.getPlayerName().isEmpty() ||
                tempPlayer.getCharacterName().isEmpty() ||
                String.valueOf(tempPlayer.getCharacterHP()).isEmpty() ||
                tempPlayer.getCharacterClass().isEmpty() ||
                tempPlayer.getCharacterWeapon().isEmpty()) {
            Toast.makeText(this, "A Field is Empty!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("ConfirmCheck", "runBack: all fields are NOT empty ");

            boolean customImageQ;
            String whatImageHere;

            //getting image data from what is the current value of the image.
            if (actualCustomPlayerImage.isEmpty()) {
                customImageQ = currentPlayerItem.isCustomImage();
                whatImageHere = currentPlayerItem.getImage();
                Log.i("imageChecks", "confirmAddCampaign1: " + whatImageHere);
            } else {
                customImageQ = true;
                whatImageHere = actualCustomPlayerImage;
                Log.i("imageChecks", "confirmAddCampaign2: " + whatImageHere);
            }

            // setting changed values to local item
            currentPlayerItem.setPlayerName(tempPlayer.getPlayerName());
            currentPlayerItem.setCharacterName(tempPlayer.getCharacterName());
            currentPlayerItem.setCharacterHP(tempPlayer.getCharacterHP());
            currentPlayerItem.setCharacterClass(tempPlayer.getCharacterClass());
            currentPlayerItem.setCharacterWeapon(tempPlayer.getCharacterWeapon());
            currentPlayerItem.setCustomImage(customImageQ);
            currentPlayerItem.setImage(whatImageHere);

            // setting this player to local
            currentCampaignItem.setPlayerToList(currentPlayerItem, playerId);

            // setting campaign to dataprovider
            DataProvider.setCampaignItem(currentCampaignItem, campaignId);

            // informing the user that the changes have been saved!
            Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void deleteAskPlayer(View view) {
        // building a popup window
        AlertDialog.Builder builder = new AlertDialog.Builder(PlayerChangerActivity.this);

        builder.setCancelable(true);
        builder.setTitle("Delete Player?");
        builder.setMessage("Do you really want to delete this Player?");

        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.setPositiveButton("Yes", (dialog, which) -> {
            currentCampaignItem.removePlayerById(playerId);
            DataProvider.setCampaignItem(currentCampaignItem, campaignId);
            finish();
        });

        // showing popup window
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);

                actualCustomPlayerImage = cursor.getString(columnIndex);
                Log.i("imageChecks", "onActivityResult: image has been set to actualCustomImage");

                cursor.close();

                // Set the Image in ImageView after decoding the String
                playerImage.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}


