package com.example.mydndapplication.manager_activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mydndapplication.R;
import com.example.mydndapplication.custom_views.GeneralCampaignAddChangeInfoView;
import com.example.mydndapplication.model.CampaignItem;
import com.example.mydndapplication.model.DataProvider;
import com.example.mydndapplication.model.Util;

import java.io.File;

public class CampaignChangerActivity extends AppCompatActivity {

    private ImageView campaignImageField;

    private GeneralCampaignAddChangeInfoView generalCampaignInfoView;

    private int campaignId;
    private String actualCustomCampaignImage = "";

    private CampaignItem currentCampaignItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign_changer);

        // getting right campaignItem
        campaignId = getIntent().getIntExtra(CampaignListViewActivity.CID, -1);
        currentCampaignItem = DataProvider.findCampaignById(campaignId);

        // finding views
        generalCampaignInfoView = findViewById(R.id.campaignAttributeFields);
        campaignImageField = findViewById(R.id.imageChangerCampaignImage);

        // Setting current values to edit Fields
        generalCampaignInfoView.setCampaignItemToFields(currentCampaignItem);

        // first checking if there IS an image
        File imgFile = new File(currentCampaignItem.getImage());
        if (currentCampaignItem.isCustomImage() && !(imgFile.exists())) {
            currentCampaignItem.setImage("noimage.jpg");
            currentCampaignItem.setCustomImage(false);
            DataProvider.setCampaignItem(currentCampaignItem, campaignId);
        }

        // setting image
        Util.setImage(currentCampaignItem, campaignImageField, this);

        // with this the image can be clicked and a new image can be added from the gallery.
        final int PICK_FROM_GALLERY = 1;
        campaignImageField.setOnClickListener(view -> {
            try {
                if (ActivityCompat.checkSelfPermission(CampaignChangerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CampaignChangerActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // this is for the back button in the top bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // this overrides the button rom the topbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void cancelCampaignEdit(View view) {
        finish();
    }

    public void confirmCampaignEdit(View view) {
        //getting field values in campaignItem format
        CampaignItem tempCampaign = generalCampaignInfoView.getFieldInputAsCampaignItem();

        // checking if fields are empty
        if (tempCampaign.getName().isEmpty() || tempCampaign.getWorldName().isEmpty() || tempCampaign.getDungeonMaster().isEmpty() || tempCampaign.getDescription().isEmpty()) {
            Toast.makeText(this, "A Field is Empty!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("ConfirmCheck", "confirm: all fields are NOT empty ");

            boolean customImageQ;
            String whatImageHere;

            // getting data from current value of image
            if (actualCustomCampaignImage.isEmpty()) {
                whatImageHere = currentCampaignItem.getImage();
                customImageQ = currentCampaignItem.isCustomImage();
                Log.i("imageChecks", "confirmAddCampaign: " + whatImageHere);
            } else {
                whatImageHere = actualCustomCampaignImage;
                customImageQ = true;
                Log.i("imageChecks", "confirmAddCampaign: " + whatImageHere);
            }

            //setting valeus to local item
            currentCampaignItem.setDescription(tempCampaign.getDescription());
            currentCampaignItem.setDungeonMaster(tempCampaign.getDungeonMaster());
            currentCampaignItem.setName(tempCampaign.getName());
            currentCampaignItem.setWorldName(tempCampaign.getWorldName());
            currentCampaignItem.setCustomImage(customImageQ);
            currentCampaignItem.setImage(whatImageHere);

            // setting local value to Dataprovider
            DataProvider.setCampaignItem(currentCampaignItem, campaignId);

            Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void deleteAskCampaign(View view) {
        //building popup window
        AlertDialog.Builder builder = new AlertDialog.Builder(CampaignChangerActivity.this);

        builder.setCancelable(true);
        builder.setTitle("Delete Campaign?");
        builder.setMessage("Do you really want to delete this Campaign? This will also delete all players playing this campaign");

        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.setPositiveButton("Yes", (dialog, which) -> {
            DataProvider.removeCampaignById(campaignId);
            finish();
        });

        // showing popup window
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);

                // setting value to class height?*
                actualCustomCampaignImage = cursor.getString(columnIndex);
                Log.i("imageChecks", "onActivityResult: image has been set to actualCustomImage");

                cursor.close();

                // Set the Image in ImageView after decoding the String
                campaignImageField.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }
}
